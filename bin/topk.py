#! /usr/bin/env python
import argparse
import heapq
import os
import sys


def topk(infiles, k, tsize):

    # Split
    split = tsize > 0
    if split:
        tfiles = [os.tmpfile() for i in range(tsize)]
        for infile in infiles:
            for line in infile:
                tfile = tfiles[hash(line) % tsize]
                tfile.write(line)
        [tfile.seek(0) for tfile in tfiles]
    else:
        tfiles = infiles

    # Process
    uniq, tops = 0, []
    ht = {}
    for tfile in tfiles:
        if split:
            ht = {}

        # Hash count
        for line in tfile:
            try:
                ht[line] += 1
            except KeyError:
                ht[line] = 1
                uniq += 1

        # Single top
        if k == 0:
            tops.extend(ht.items())
        else:
            tops.extend(heapq.nlargest(k, ht.items(), lambda x: x[1]))

    # Heap-Sort
    if k == 0:
        tops.sort(key=lambda x: x[1], reverse=True)
        result = tops
    else:
        result = heapq.nlargest(k, tops, lambda x: x[1])

    return result, uniq


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Top K')

    parser.add_argument('-k', type=int, default=20)
    parser.add_argument('-t', type=int, default=0)
    parser.add_argument('files', type=file, nargs='*', default=[sys.stdin])

    args = parser.parse_args()

    try:
        result, uniq = topk(args.files, k=args.k, tsize=args.t)
    except KeyboardInterrupt:
        pass
    else:
        print 'Uniq count: %d' % uniq
        for line, count in result:
            print '%d\t%s' % (count, line.strip())
