#! /usr/bin/env bash

# Arguments
if [ -z "$1" ]; then
    echo "usage: $0 <host>"
    exit 1
fi
host=$1

# Generate key if not exists
pubkey="$HOME/.ssh/id_rsa.pub"
if [ ! -f $pubkey ]; then
    ssh-keygen
fi
keycon=`cat $pubkey`
if [ -z "$keycon" ]; then
    echo "pubkey $pubkey invalid"
    exit 2
fi

# Push it
if [ -n "$2" ]; then
    ak_remote="$2/.ssh/authorized_keys"
else
    ak_remote="$HOME/.ssh/authorized_keys"
fi
ssh $host "mkdir -m700 -p `dirname $ak_remote`; echo $keycon >> $ak_remote; chmod 600 $ak_remote;"
