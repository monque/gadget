#! /usr/bin/env python
import argparse
import os
import sys
import time


def main(left):
    remind_list = []
    while left > 5:
        remind_list.append(5)
        left -= 5
    if left > 0:
        remind_list.append(left)

    display('be focus...')
    elapse = 0
    for minutes in remind_list:
        for i in range(minutes):
            time.sleep(60)
            elapse += 1
            display('elapse %d minutes' % elapse, False)
        display('elapse %d minutes' % elapse)
    display('time up, focused for %d minutes' % elapse)


def display(msg, voice=True):
    print(msg)
    if voice:
        os.system('say -v Samantha ' + msg)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Be Focus')

    parser.add_argument('minutes', type=int)

    args = parser.parse_args()

    begin = time.time()
    try:
        main(args.minutes)
    except KeyboardInterrupt:
        elapse = (time.time() - begin) / 60
        display('terminate, focused for %d minutes' % elapse)
