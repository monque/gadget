#! /usr/bin/env python
import random


class Slot(object):

    def __init__(self):
        self.set_empty()

    def set_empty(self):
        self.val = 0

    def is_empty(self):
        return self.val == 0

    def power(self):
        if self.val < 2:
            return 0
        n = 0
        val = self.val
        while val:
            n += 1
            val >>= 1
        return n


class Board(object):

    def __init__(self, size=4):
        self.size = size
        self.moves = 0
        self.score = 0
        self.matrix = []
        for x in range(self.size):
            line = [Slot() for y in range(self.size)]
            self.matrix.append(line)

    def grow(self):
        slot = random.choice(self.list_empty())
        if random.randrange(10):
            slot.val = 2
        else:
            slot.val = 4
        self.score += slot.val

    def list(self):
        for line in self.matrix:
            for slot in line:
                yield slot

    def list_empty(self):
        return [s for s in self.list() if s.is_empty()]

    def list_duplicated(self):
        slots = []
        for y in range(self.size - 1):
            for x in range(self.size - 1):
                slot = self.matrix[y][x]
                right = self.matrix[y][x + 1]
                down = self.matrix[y + 1][x]
                if slot.val == right.val or slot.val == down.val:
                    slots.append(slot)

        # Bottom-right
        slot = self.matrix[y + 1][x + 1]
        if slot.val == right.val or slot.val == down.val:
            slots.append(slot)

        return slots

    def list_line(self, direction, a):
        if direction == 'up':
            line = [self.matrix[y][a] for y in range(self.size)]
        elif direction == 'down':
            line = [self.matrix[y][a] for y in reversed(range(self.size))]
        elif direction == 'left':
            line = self.matrix[a][:]
        elif direction == 'right':
            line = [self.matrix[a][x] for x in reversed(range(self.size))]
        return line

    def shift(self, direction, a):
        modified = False
        line = self.list_line(direction, a)
        for i in range(self.size - 1):
            # Find empty slot
            if not line[i].is_empty():
                continue
            # Find next non-empty slot
            for j in range(i, self.size):
                if not line[j].is_empty():
                    break
            if line[j].is_empty():
                break
            # Shift it
            line[i].val, line[j].val = line[j].val, line[i].val
            modified = True
        return modified

    def merge(self, direction, a):
        modified = False
        line = self.list_line(direction, a)
        for i in range(self.size - 1):
            # Find empty slot
            if line[i].is_empty():
                continue
            # Merge next
            if line[i].val == line[i + 1].val:
                line[i].val += line[i + 1].val
                line[i + 1].set_empty()
                modified = True
        return modified

    # Public interface
    def display(self):
        bc_res = '\x1b[0m'
        bc_list = [
            '\x1b[0;31m',  # red
            '\x1b[0;32m',  # gre
            '\x1b[0;33m',  # yel
            '\x1b[0;34m',  # blu
            '\x1b[0;35m',  # mag
            '\x1b[0;36m',  # cya
            '\x1b[0;37m',  # whi
            '\x1b[1;31m',  # red
            '\x1b[1;32m',  # gre
            '\x1b[1;33m',  # yel
            '\x1b[1;34m',  # blu
            '\x1b[1;35m',  # mag
            '\x1b[1;36m',  # cya
            '\x1b[1;37m',  # whi
        ]

        print 'moves:', self.moves, 'score:', self.score
        for line in self.matrix:
            for slot in line:
                if slot.power() > 0:
                    bc = bc_list[slot.power() % len(bc_list)]
                else:
                    bc = ''
                print '%s%4s%s' % (bc, slot.val, bc_res),
            print ''
        print '-' * (self.size * 5 - 1)

    def set(self, text):
        vals = []
        for line in text.split('\n'):
            line = line.strip()
            if not line:
                continue
            for val in line.split():
                val = int(val)
                vals.append(val)
        vals.reverse()

        for line in self.matrix:
            for slot in line:
                slot.val = vals.pop()
                self.score += slot.val

    def begin(self):
        self.grow()
        self.grow()

    def is_dead(self):
        return not (self.list_empty() or self.list_duplicated())

    def move(self, direction):
        modified = False
        for a in range(self.size):
            if self.shift(direction, a):
                modified = True
            if self.merge(direction, a):
                modified = True
            if self.shift(direction, a):
                modified = True

        if modified:
            self.grow()
            self.moves += 1
            return True
        else:
            return False


def play(size):
    board = Board(size=size)
    board.begin()
    # board.set('2 16\n4 8')

    while not board.is_dead():
        board.display()
        direction = raw_input('move [U]p, [D]own, [L]eft, [R]ight: ').lower()

        if direction in ['u', 'up']:
            direction = 'up'
        elif direction in ['d', 'down']:
            direction = 'down'
        elif direction in ['l', 'left']:
            direction = 'left'
        elif direction in ['r', 'right']:
            direction = 'right'
        else:
            print 'wrong direction'
            continue
        board.move(direction)

    board.display()
    raw_input('you are dead')

def play_ai():
    board = Board(size=3)
    board.begin()
    while not board.is_dead():
        if random.randrange(2):
            steps = ['right', 'up', 'down', 'left']
        else:
            steps = ['right', 'down', 'up', 'left']

        for direction in steps:
            moved = board.move(direction)
            if moved:
                break
    return board

if __name__ == '__main__':
    # board_max = None
    # for x in range(100):
    #     board = play_ai()
    #     if board_max is None or board.score > board_max.score:
    #         board_max = board
    # board_max.display()
    try:
        play(size=4)
    except KeyboardInterrupt:
        print 'bye'
