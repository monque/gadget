#! /usr/bin/env ruby

class AttachCastle
    attr_accessor :allow_actions

    :action
    :detail

    :begin_time
    :end_time

    def working_on(action, detail)
        @action = action
        @detail = detail

        if not @allow_actions.include?(action.downcase)
            raise "Invalid action \"#{action}\""
        end

        self.work_begin
        begin
            loop do
                sleep 10
                elapse = (Time.new - @begin_time).to_i / 60
                puts "Elapse #{elapse} minutes"
            end
        rescue SystemExit, Interrupt
            self.work_end
        end
    end

    def work_begin
        @begin_time = Time.new
        puts "Begin working at #{@begin_time.strftime "%H:%M"} [#{self.class} #{@action} #{@detail}]"
    end

    def work_end
        @end_time = Time.new
        puts "Work done at #{@end_time.strftime "%H:%M"} [#{self.class} #{@action} #{@detail}]"

        self.write_log
    end

    def write_log
        open(ENV['HOME'] + '/.monque/attack.log', 'a') { |f|
            f.puts "#{@begin_time.strftime "%m-%d\t%H:%M"} - #{@end_time.strftime "%H:%M"}\t#{self.class}.#{@action}\t#{@detail}"
        }
    end
end

# 工作以外的
class Private < AttachCastle
    def initialize
        @allow_actions = [
            'eat',      # 吃饭
            'rest',     # 休息
            'common',   # 日常通用（接水、上厕所等）
        ]
    end
end

# 工作中的个人行为
class Self < AttachCastle
    def initialize
        @allow_actions = [
            'dev',      # 开发
            'learn',    # 学习
            'think',    # 思考
        ]
    end
end

# 业务相关
class Business < AttachCastle
    def initialize
        @allow_actions = [
            'discus',   # 讨论
            'meeting',  # 开会
            'talk',     # 谈话、聊天
        ]
    end
end

# 技术相关
class Tech < AttachCastle
    def initialize
        @allow_actions = [
            'share',    # 分享
            'discus',   # 讨论
            'meeting',  # 开会
            'talk',     # 谈话、聊天
        ]
    end
end

# 团队相关
class Team < AttachCastle
    def initialize
        @allow_actions = [
            'meeting',  # 开会
            'talk',     # 谈话、聊天
        ]
    end
end

# Main
case ARGV.length
    when 1
        klass = ARGV[0].capitalize
        puts "klass \"#{klass}\" allows #{Object.const_get(klass).new.allow_actions} actions"

    when 3
        klass = ARGV[0].capitalize
        action = ARGV[1].capitalize
        detail = ARGV[2]
        Object.const_get(klass).new.working_on(action, detail)

    else
        puts "Usage: #{$0} {private|self|business|test|team} <action> <detail>"
end
