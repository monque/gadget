#!/bin/sh

d() { echo `date +'%Y-%m-%d %H:%M:%S'` $@; }
r() { d $@; $@; ret=$?; }

TOKEN=""
PREVIOUS_CACHE="/tmp/ddns_dnspod_previous"

# List all records
# curl -s -X POST https://dnsapi.cn/Record.List -d "login_token=$TOKEN&format=json&domain=mo47.com" | jq; exit

update_ddns() {
    record_id=$1
    sub_domain=$2

    r curl -s -X POST https://dnsapi.cn/Record.Info -d "login_token=$TOKEN&format=json&domain=mo47.com&record_id=$record_id"; echo
    r curl -X POST https://dnsapi.cn/Record.Ddns -d "login_token=$TOKEN&format=json&domain=mo47.com&record_id=$record_id&sub_domain=$sub_domain&record_line_id=0"; echo
}

main()
{
    # Detect current IP
    current=`curl --connect-timeout 5 -s https://qifu-api.baidubce.com/ip/local/geo/v1/district | jq -r '.ip'`
    if [ -z "$current" ]; then
        d "Detect current IP failed"
        exit
    fi
    if ! [[ $current =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
        d "Detect current IP verify failed, response: $current"
        exit
    fi

    # Compare previous
    previous=`cat $PREVIOUS_CACHE`
    if [ "$current" == "$previous" ]; then
        d "IP $current has not changed"
        exit
    fi

    # Update DDNS
    d "IP changed $previous -> $current"
    echo $current > $PREVIOUS_CACHE

    update_ddns 405666726 dongba

}

main
