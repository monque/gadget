#! /usr/bin/env python3
from glob import glob
import argparse
import hashlib
import os
import sys
import time
import zlib


context = {
    'verbose': False,
    'blksize': 4096,
}
D = {
    'size_cnt': 0,
    'size_time': 0.0,
    'quick_cnt': 0,
    'quick_time': 0.0,
    'quick_read_cnt': 0,
    'quick_read_time': 0.0,
    'quick_read_size': 0,
    'full_cnt': 0,
    'full_time': 0.0,
    'full_read_cnt': 0,
    'full_read_time': 0.0,
    'full_read_size': 0,

    'read_cnt': 0,
    'read_time': 0.0,
    'read_size': 0,
    'crc32_cnt': 0,
    'crc32_time': 0.0,
    'crc32_size': 0,
    'sha_cnt': 0,
    'sha_time': 0.0,
    'sha_size': 0,
}


def _read(fp, *args):
    if context['verbose']:
        time_begin = time.time()
        D['read_cnt'] += 1

    content = fp.read(*args)

    if context['verbose']:
        D['read_time'] += time.time() - time_begin
        D['read_size'] += len(content)

    return content


def _crc32(content):
    if context['verbose']:
        time_begin = time.time()
        D['crc32_cnt'] += 1

    csum = zlib.crc32(content)

    if context['verbose']:
        D['crc32_time'] += time.time() - time_begin
        D['crc32_size'] += len(content)

    return csum


def _sha256(content):
    if context['verbose']:
        time_begin = time.time()
        D['sha_cnt'] += 1

    csum = hashlib.sha256(content).digest()

    if context['verbose']:
        D['sha_time'] += time.time() - time_begin
        D['sha_size'] += len(content)

    return csum


def eprint(*args):
    print(*args, file=sys.stderr)


def main(paths, lower_limit=0):

    # Size check
    time_begin = time.time()
    sdups = {}
    while paths:
        files = glob(paths.pop() + '/*')
        for fname in files:
            if os.path.islink(fname):
                continue
            elif os.path.isdir(fname):
                paths.append(fname)
                continue

            fsize = os.stat(fname).st_size
            if fsize == 0:
                continue

            D['size_cnt'] += 1

            if fsize not in sdups:
                sdups[fsize] = [fname]
            else:
                sdups[fsize].append(fname)

    if context['verbose']:
        D['size_time'] = time.time() - time_begin

    # Quick check
    time_begin = time.time()
    qdups = {}
    fsums = {}
    for fsize, files in sdups.items():
        if len(files) < 2:
            continue

        for fname in files:
            try:
                qsum, fsum = checksum(fname, fsize)
            except Exception as e:
                eprint('Checksum failed,', e)
                continue

            D['quick_cnt'] += 1

            key = (qsum, fsize)
            if key not in qdups:
                qdups[key] = [fname]
            else:
                qdups[key].append(fname)

            if fsum is not None:
                fsums[fname] = fsum

    if context['verbose']:
        D['quick_time'] = time.time() - time_begin
        D['quick_read_cnt'] = D['read_cnt']
        D['quick_read_time'] = D['read_time']
        D['quick_read_size'] = D['read_size']

    # Full check
    time_begin = time.time()
    fdups = {}
    for (qsum, fsize), files in qdups.items():
        if len(files) < 2:
            continue

        for fname in files:
            if fname in fsums:
                fsum = fsums[fname]
            else:
                try:
                    fsum = checksum(fname, None)[1]
                except Exception as e:
                    eprint('Checksum failed,', e)
                    continue

            D['full_cnt'] += 1
            key = (fsum, fsize)
            if key not in fdups:
                fdups[key] = [fname]
            else:
                fdups[key].append(fname)

        # Show errors for debuging
        if context['verbose'] and False and len(qdups[(qsum, fsize)]) != len(fdups[(fsum, fsize)]):
            eprint("Quick errors")
            for fname in qdups[qsum]:
                eprint('q', fname)
            for fname in fdups[fsum]:
                eprint('f', fname)

    if context['verbose']:
        D['full_time'] = time.time() - time_begin
        D['full_read_cnt'] = D['read_cnt'] - D['quick_read_cnt']
        D['full_read_time'] = D['read_time'] - D['quick_read_time']
        D['full_read_size'] = D['read_size'] - D['quick_read_size']

    # Filter result
    dupcnt = dupsize = redundantsize = 0
    for (fsum, fsize), files in fdups.items():
        if len(files) < 2:
            continue

        fcnt = len(files)
        dupcnt += fcnt
        dupsize += fsize * fcnt
        redundantsize += fsize * (fcnt - 1)

        if fsize < lower_limit:
            continue

        # Output
        print('%d * %.2fm' % (fcnt, fsize / 1048576))
        for fname in files:
            print(fname)
        print('')

    print('Duplicated files: %d %.2fm %.2fm' % (dupcnt, dupsize / 1048576, redundantsize / 1048576))

    if context['verbose']:
        quick_converage = dupcnt / D['full_cnt'] * 100 if D['full_cnt'] != 0 else 100

        eprint('Duplicated files: %d %.2fm %.2fm' % (dupcnt, dupsize / 1048576, redundantsize / 1048576))
        eprint('Size check:  %7d %6.1fs' % (D['size_cnt'], D['size_time']))
        eprint('Quick check: %7d %6.1fs %7.1fm %7.1fm/s %7d' % (D['quick_cnt'], D['quick_time'], D['quick_read_size'] / 1048576, D['quick_read_size'] / 1048576 / D['quick_read_time'], D['quick_read_cnt']))
        eprint('Full check:  %7d %6.1fs %7.1fm %7.1fm/s %7d %.2f%%' % (D['full_cnt'], D['full_time'], D['full_read_size'] / 1048576, D['full_read_size'] / 1048576 / D['full_read_time'], D['full_read_cnt'], quick_converage))

        eprint('Read:        %7d %6.1fs %7.1fm %7.1fm/s' % (D['read_cnt'], D['read_time'], D['read_size'] / 1048576, D['read_size'] / 1048576 / D['read_time']))
        eprint('CRC32:       %7d %6.1fs %7.1fm %7.1fm/s' % (D['crc32_cnt'], D['crc32_time'], D['crc32_size'] / 1048576, D['crc32_size'] / 1048576 / D['crc32_time']))
        eprint('SHA:         %7d %6.1fs %7.1fm %7.1fm/s' % (D['sha_cnt'], D['sha_time'], D['sha_size'] / 1048576, D['sha_size'] / 1048576 / D['sha_time']))


def checksum(fname, fsize):
    fp = open(fname, 'rb', buffering=context['blksize'])

    # Full-check
    if fsize is None:
        return None, _sha256(_read(fp))

    # Full-check directly for small files
    if fsize <= context['blksize']:
        csum = _sha256(_read(fp))
        return csum, csum

    # Optimize method
    content = _read(fp, context['blksize'])

    # The limit file size for tail-reading is absolute a magic number
    # if the number is low, such as 2, when process some small files
    # result is no different between reading full-content and head-content
    if fsize > context['blksize'] * checksum.trlimit:
        time_begin = time.time()
        fp.seek(-context['blksize'], os.SEEK_END)
        content += _read(fp, context['blksize'])

    return _crc32(content), None


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Find duplicate files')

    parser.add_argument('paths', nargs='+')
    parser.add_argument('-V', '--verbose', action='store_true')
    parser.add_argument('--block_size', type=int, default=4096)
    parser.add_argument('--tread_limit', type=int, default=20)
    parser.add_argument('--lower_limit', type=str)

    args = parser.parse_args()

    lower_limit = 0
    if args.lower_limit is not None:
        try:
            unit = args.lower_limit[-1].lower()
            lower_limit = int(args.lower_limit[:-1])
            if unit == 'k':
                lower_limit *= 1024
            elif unit == 'm':
                lower_limit *= pow(1024, 2)
            elif unit == 'g':
                lower_limit *= pow(1024, 3)
        except:
            pass

    paths = []
    for path in args.paths:
        if os.path.isdir(path):
            paths.append(os.path.realpath(path))

    context['verbose'] = args.verbose
    context['blksize'] = args.block_size
    checksum.trlimit = args.tread_limit

    try:
        main(paths[:], lower_limit)
    except KeyboardInterrupt:
        pass
