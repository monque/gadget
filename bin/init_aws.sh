#################### User ####################

create_auth_keys() {
    mkdir -m 700 /home/$1/.ssh
    touch /home/$1/.ssh/authorized_keys
    chmod 600 /home/$1/.ssh/authorized_keys
    chown -R $1:$1 /home/$1/.ssh
}

# monque
useradd -u 501 monque
create_auth_keys monque
echo "monque ALL=(ALL) ALL" >> /etc/sudoers
passwd monque

# proxy, en
useradd -u 502 -s /sbin/nologin proxy
create_auth_keys proxy
useradd -u 503 ben


#################### System ####################

# Timezone
echo -e "ZONE=\"Asia/Shanghai\"\nUTC=false" > /etc/sysconfig/clock
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

# Swap
if [ ! -f /var/swap ]; then
    echo "Prepare swap file"
    dd if=/dev/zero of=/var/swap bs=1MB count=512
    mkswap /var/swap
    swapon /var/swap
    echo "/var/swap   swap        swap    defaults        0   0" >> /etc/fstab
fi


#################### SSH ####################

echo "AllowUsers ec2-user monque proxy" >> /etc/ssh/sshd_config
systemctl restart sshd.service
