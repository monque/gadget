#################### Dependency ####################
sudo yum install -yq tmux git zsh

# Check git exists
git version > /dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "git does not exists"
    exit
fi


#################### Gadget ####################
git clone https://bitbucket.org/monque/gadget ~/.monque
ln -s ~/.monque/dots/dot_tmux.conf ~/.tmux.conf
echo -e "[include]\n    path = ~/.monque/dots/dot_gitconfig" >> ~/.gitconfig
echo "source ~/.monque/dots/vim_vundle.vim" >> ~/.vimrc
echo "source ~/.monque/dots/vim_rc.vim" >> ~/.vimrc
echo "source ~/.monque/dots/sh_rc.sh" >> ~/.bashrc

# vim Vundle
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

# Zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
echo "source ~/.monque/dots/sh_rc.sh" >> ~/.zshrc
