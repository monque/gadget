(provide 'emacs_rc)

(prefer-coding-system 'utf-8)

;; Layout
(setq inhibit-startup-message t)
(show-paren-mode t)
(column-number-mode t)

;; Color
;; (set-foreground-color "#F8F8F2")
;; (set-background-color "#1B1D1E")

;; Disable auto backup
(setq make-backup-files nil)

;; Tab
(setq indent-tabs-mode nil
      tab-always-indent nil
      tab-width 4)

;; 页面平滑滚动,靠近屏幕边沿3行时开始滚动，可以很好的看到上下文。
(setq scroll-margin 3
      scroll-conservatively 10000)

;; Font
(set-face-attribute 'default nil :font "Monaco 9")
(dolist (charset '(kana han symbol cjk-misc bopomofo))
  (set-fontset-font (frame-parameter nil 'font)
                    charset (font-spec :family "WenQuanYi Micro Hei Mono"
                                       :size 14)))
;;(setq face-font-rescale-alist '(("WenQuanYi Micro Hei Mono" . 1.2)))

