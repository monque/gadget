# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output. So make sure this doesn't display
# anything or bad things will happen !

# Test for an interactive shell. There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.

# If not running interactively, don't do anything!
[[ $- != *i* ]] && return


# Determine shell
if [ -n "$ZSH_NAME" ]; then
    # Prompt
    local ret_status="%(?::%{$fg_bold[red]%}:( %s)%{$fg_bold[blue]%}$"
    PROMPT='%{$fg_bold[green]%}%n@%m %{$fg_bold[blue]%}%~ $(git_prompt_info)${ret_status}%{$fg_bold[green]%}%p %{$reset_color%}'

    ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[cyan]%}git:(%{$fg[red]%}"
    ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
    ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[cyan]%}) %{$fg[yellow]%}✗%{$reset_color%}"
    ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[cyan]%})"

    # Options
    unsetopt SHARE_HISTORY
else
    # Inspired by Archlinux
    # https://wiki.archlinux.org/index.php/Color_Bash_Prompt

    # Bash won't get SIGWINCH if another process is in the foreground.
    # Enable checkwinsize so that bash will check the terminal size when
    # it regains control.
    # http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
    shopt -s checkwinsize

    # Enable history appending instead of overwriting.
    shopt -s histappend

    # Enable colors for ls, etc. Prefer ~/.dir_colors
    if type -P dircolors >/dev/null ; then
        if [[ -f ~/.dir_colors ]] ; then
            eval $(dircolors -b ~/.dir_colors)
        elif [[ -f /etc/DIR_COLORS ]] ; then
            eval $(dircolors -b /etc/DIR_COLORS)
        fi
    fi


    # Prompt
    PS1="$(if [[ ${EUID} == 0 ]]; then echo '\[\033[01;31m\]\h'; else echo '\[\033[01;32m\]\u@\h'; fi)\[\033[01;34m\] \w \$([[ \$? != 0 ]] && echo \"\[\033[01;31m\]:(\[\033[01;34m\] \")\\$\[\033[00m\] "
    # Use this other PS1 string if you want \W for root and \w for all other users:
    # PS1="$(if [[ ${EUID} == 0 ]]; then echo '\[\033[01;31m\]\h\[\033[01;34m\] \W'; else echo '\[\033[01;32m\]\u@\h\[\033[01;34m\] \w'; fi) \$([[ \$? != 0 ]] && echo \"\[\033[01;31m\]:(\[\033[01;34m\] \")\\$\[\033[00m\] "
    PS2="> "
    PS3="> "
    PS4="+ "
fi


# Environment
export LANG="en_US.UTF-8"
export EDITOR="vim"


# We have colors :-)
monque_256_test() {
    for (( i = 0; i < 256; i++)); do
        echo -e "\x1B[38;5;"${i}"m.\c"
        if [ $(((${i} + 1) % 64)) -eq 0 ]; then
            echo ""
        fi
    done
    echo -e "\x1B[0m\c"
}
monque_256_test


# Manual page
man() {
    env LESS_TERMCAP_mb=$'\x1B[01;31m' \
    LESS_TERMCAP_md=$'\x1B[01;38;5;74m' \
    LESS_TERMCAP_me=$'\x1B[0m' \
    LESS_TERMCAP_se=$'\x1B[0m' \
    LESS_TERMCAP_so=$'\x1B[38;5;246m' \
    LESS_TERMCAP_ue=$'\x1B[0m' \
    LESS_TERMCAP_us=$'\x1B[04;38;5;146m' \
    man "$@"
}


# Alias
SYSNAME=`uname -s`
if [ $SYSNAME = "Darwin" ]; then
    alias ls="ls -G"
    alias ll="ls -l -G"
else
    alias ls="ls --color=auto"
    alias ll="ls -l --color=auto"
    alias dmesg='dmesg --color'
fi
echo 'g' | grep g --color=auto >/dev/null 2>&1 && alias grep="grep --color=auto"

# Done
echo -e "\x1B[1;32m[load] \x1B[1;33msh_rc\x1B[0m"


# Load local.sh
if [ -n "$ZSH_NAME" ]; then
    sh_local="`dirname $0`/sh_local.sh"
else
    sh_local="`dirname ${BASH_SOURCE[0]}`/sh_local.sh"
fi
if [ -e "$sh_local" ]; then
    source $sh_local
    echo -e "\x1B[1;32m[load] \x1B[1;33msh_local\x1B[0m"
fi
unset sh_local
