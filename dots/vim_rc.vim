" ****************************** System ******************************
set     nocompatible
set     encoding        =utf-8
set     fileencodings   =utf-8,chinese,cp936,euc-cn,latin1
lang    messages        en_US.UTF-8 " for gvim in Windows

" ****************************** Display ******************************
syntax  on
silent!color            molokai " https://github.com/tomasr/molokai

" ****************************** Layout ******************************
set     nonumber
set     ruler
set     tabpagemax      =99
set     showtabline     =1
set     showcmd
set     laststatus      =2
silent!set     colorcolumn     =79
set     nowrap

" ****************************** GUI ******************************
if has("gui_gtk2")
    set guifont         =Monaco\ 9
    set guifontwide     =WenQuanYi\ Micro\ Hei\ Mono\ 9
elseif has("mac")
    set guifont         =Monaco:h11
    " Default font is good enough
else
    set guifont         =Monaco:h9,Consolas:h9
    set guifontwide     =YaHei_Consolas_Hybrid:h9
endif
set     guioptions      = " egmrLtT

" ****************************** Editor ******************************
" Format
set     tabstop         =4
set     shiftwidth      =4
set     listchars       =tab:>-,eol:<,trail:-,nbsp:%,extends:>,precedes:<
set     nolist
set     expandtab
set     autoindent
set     smartindent
set     smarttab
set     shiftround

" Search
set     hlsearch
set     incsearch
set     ignorecase
set     smartcase
set     tags            =~/.vim/tags/**/tags,./tags,tags " The filename itself cannot contain wildcards, it is used as-is.

" Behaviour
set     nobackup
set     noswapfile
set     backspace       =indent,eol,start
set     selectmode      =key
set     mouse           =
set     wildignore     +=*.o,*~,*.pyc
set     formatoptions  +=m

" Fold
set     foldmethod      =indent
set     foldlevel       =100

filetype plugin indent on

" ****************************** cscope ******************************
if !has('nvim')
    set csto=0
    set cst
    set nocsverb
    if filereadable("cscope.out")
        cs add cscope.out
    endif
    set csverb
endif

" 3 or c: Find functions calling this function
nmap gc :cs find c <C-R>=expand("<cword>")<CR><CR>

" ****************************** Keymap ******************************
map     <M-!>           1gt
map     <M-@>           2gt
map     <M-#>           3gt
map     <M-$>           4gt
map     <M-%>           5gt
map     <M-^>           6gt
map     <M-&>           7gt
map     <M-*>           8gt
map     <M-(>           9gt

" tips from http://vim.wikia.com/wiki/Get_Alt_key_to_work_in_terminal
map     !             1gt
map     @             2gt
map     #             3gt
map     $             4gt
map     %             5gt
map     ^             6gt
map     &             7gt
map     *             8gt
map     (             9gt

" for gvim on OS X
map     <D-!>           1gt
map     <D-@>           2gt
map     <D-#>           3gt
map     <D-$>           4gt
map     <D-%>           5gt
map     <D-^>           6gt
map     <D-&>           7gt
map     <D-*>           8gt
map     <D-(>           9gt

" :W sudo saves the file (useful for handling the permission-denied error)
command W w !sudo tee % > /dev/null

" Strip tailing white space
command StripTailing %s/\s\+$//ge

" Leader key
let mapleader = ","
let g:mapleader = ","

" Disable highlight
nmap <silent> <leader>/ :noh<cr>

" Toggle paste mode
nmap <leader>p :set paste!<cr>

" Toggle list mode
nmap <leader>l :set list!<cr>

" Toggle wrap mode
nmap <leader>w :set wrap!<cr>

" Re-diff
nmap <silent> <leader>du :diffupdate<cr>
nmap <silent> <leader>do :diffthis<cr>
nmap <silent> <leader>df :diffoff<cr>

" Quit
nmap <leader>q :q<cr>
nmap <leader>qa :qa<cr>

" ****************************** Plugin ******************************
" NerdCommenter
let NERDSpaceDelims = 1

" Airline
let g:airline_symbols_ascii = 1
