#include "devutils.h"

int is_little_endian(void)
{
    int i;
    unsigned char *p;

    i = 1;
    p = (unsigned char *) &i;

    showBits(i);

    return *p; /* little endian: lower bit come first */
}

int main(int argc, char *argv[])
{
    printf("is little endian: %d\n", is_little_endian());
}
