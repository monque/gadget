#include "devutils.h"
#include <assert.h>
#include <limits.h>

int threefourths_double(int x) {
    return (int) (0.75 * x);
}

/* Calculate (3 / 4 * x) */
int threefourths(int x) {

    !(x & INT_MIN) && (x = x - (((x - 1) >> 2) + 1));
     (x & INT_MIN) && (x = x - (x >> 2));

    return x;
}

int main() {
    int i;

    for (i = -10; i <= 10; i++) {
        printf("%3d: %5.2f %2d %2d : %2d\n", i, 0.75 * i, threefourths_double(i), threefourths(i), i >> 2);
    }

    for (i = -1000; i < 1000; i++) {
        assert(threefourths_double(i) == threefourths(i));
    }
    assert(threefourths(INT_MAX) == 1610612735);
    assert(threefourths(INT_MAX) == threefourths_double(INT_MAX));
    assert(threefourths(INT_MIN) == threefourths_double(INT_MIN));
    assert(threefourths(0) == threefourths_double(0));
}
