#include "devutils.h"

#define W (sizeof(int) * 8)

unsigned srl(unsigned x, int k) {
    /* Perform shift arithmetically */
    unsigned xsra = (int) x >> k;

    return xsra & (1 << (W - k)) - 1;
}

int sra(int x, int k) {
    /* Perform shift logically */
    int xsrl = (unsigned) x >> k;

    return xsrl | ~((1 << (W - k)) - 1);
}

int main()
{
    int x;
    unsigned u;

    u = 0xFF000000;
    showBits(u);
    u = srl(u, 8);
    showBits(u);

    x = 0xFF000000;
    showBits(x);
    x = sra(x, 8);
    showBits(x);
}
