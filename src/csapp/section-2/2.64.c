#include "devutils.h"

/* Return 1 when any odd bit of x equals 1; 0 otherwise. Assume
 * w = 32 */
int any_odd_one(unsigned x) {
    return !!(0xAAAAAAAA & x);
}

int main() {
    int i;

    for (i = 0; i < 16; i++) {
        showBits(i);
        printf("%d has odd bit equals 1: %d\n", i, any_odd_one(i));
    }

    printf("%d has odd bit equals 1: %d\n", -1, any_odd_one(-1));
}
