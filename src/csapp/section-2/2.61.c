#include "devutils.h"

/* 竟然忘了 ! 这个常用的操作符。。。*/
int is_all_bit_1(int x)
{
    /* x的任何位都等于1 */
    x = 0x0000FFFF & x & (x >> 16);
    x = 0x000000FF & x & (x >> 8);
    x = 0x0000000F & x & (x >> 4);
    x = 0x00000003 & x & (x >> 2);
    x = 0x00000001 & x & (x >> 1);

    return x;
}

int main()
{
    int x;

    /* x的任何位都等于1 */
    !~x;

    /* x的任何位都等于0 */
    !x;

    /* x的最低有效字节中的位都等于1 */
    !~(x | ~0xFF);

    /* x的最高有效字节中的位都等于0 */
    !(x && (0xFF << (sizeof(int) - 1)))
}
