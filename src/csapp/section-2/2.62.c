#include "devutils.h"

int int_shifts_are_arithmetic(void)
{
    int x;

    x = 0xF0000000;
    x = x >> 4 & 0xF0000000;

    return !!x;
}

int main()
{
    int x;
    unsigned u;

    /* 逻辑右移 */
    x = 0xF0000000 >> 4;
    showBits(x);

    /* 算数右移 */
    x = 0xF0000000;
    x >>= 4;
    showBits(x);

    /* 逻辑右移 */
    u = 0xF0000000;
    u >>= 4;
    showBits(u);

    printf("int shifts are arithmetic: %d\n", int_shifts_are_arithmetic());
}
