#include "devutils.h"
#include <assert.h>
#include <limits.h>

/* Determine whether arguments can be subtracted without overflow
 * 如果计算x - y不溢出，这个函数返回1 */
int tsub_ok(int x, int y) {
    int sum, over_pos, over_neg;

    sum = x - y;

    over_pos = !(x & INT_MIN) && (y & INT_MIN) && (sum & INT_MIN);
    over_neg = (x & INT_MIN) && !(y & INT_MIN) && !(sum & INT_MIN);

    return !(over_pos || over_neg);
}

int main() {
    assert(tsub_ok(INT_MAX, -1) == 0);

    assert(tsub_ok(INT_MAX, 1) == 1);

    assert(tsub_ok(INT_MIN, 1) == 0);

    assert(tsub_ok(INT_MIN, -1) == 1);
}
