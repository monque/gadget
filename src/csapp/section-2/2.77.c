#include "devutils.h"
#include <assert.h>
#include <limits.h>

int main() {
    int x = 1;

    assert(17 == (x << 4) + x);
    assert(-7 == x - (x << 3));
    assert(60 == (x << 6) - (x << 2));
    assert(-112 == (x << 4) - (x << 7));

    return 0;
}
