#include "devutils.h"
#include <assert.h>
#include <limits.h>

/* 返回x*y中的高w位 */
int signed_high_prod(int x, int y) {
    long r = (long) x * y;
    return r >> 32;
}

int test_signed(int x, int y) {
    int i = signed_high_prod(x, y);
    printf("%d * %d = (%d << 32) + %u\n", x, y, i, (unsigned) (x * y));
    return i;
}

unsigned unsigned_high_prod(unsigned x, unsigned y) {
    /**
     * X * Y    = (X*Y)mod2w + high_signed() * 2^w
     * X' * Y'  =  X*Y + (Xw-1*Y + Yw-1*X) * 2^w + (Xw-1 * Yw-1) * 2^2w
     *          = (X*Y)mod2w + high_signed() * 2^w
     *                       + (Xw-1*Y + Yw-1*X) * 2^w + (Xw-1 * Yw-1) * 2^2w
     * X * Y 与 X' * Y'有相同位级表示，可推出
     * hign_unsigned() = (high_signed() * 2^w + (Xw-1*Y + Yw-1*X) * 2^w + (Xw-1 * Yw-1) * 2^2w) / 2^w mod2w
     *                 = (high_signed() + (Xw-1*Y + Yw-1*X) + (Xw-1 * Yw-1) * 2^w) mod2w
     *                 =  high_signed() + (Xw-1*Y + Yw-1*X)
     */
    return signed_high_prod(x, y) + x * (y >> 31) + y * (x >> 31);
}

unsigned unsigned_high_prod_contrast(unsigned x, unsigned y) {
    unsigned long r = (unsigned long) x * y;
    return r >> 32;
}

unsigned test_unsigned(unsigned x, unsigned y) {
    unsigned u, uc;

    u = unsigned_high_prod(x, y);
    uc = unsigned_high_prod_contrast(x, y);
    if (u != uc) {
        printf("%u != %u\n", u, uc);
        printf("%u * %u = (0x%X << 32) + %u u\n", x, y, u, x * y);
        printf("%u * %u = (0x%X << 32) + %u uc\n", x, y, uc, x * y);
    } else {
        printf("%u * %u = (0x%X << 32) + %u\n", x, y, u, x * y);
    }

    return u;
}

int main() {
    /* assert(test_signed(99, 10) == 0); */
    /* assert(test_signed(INT_MAX, 10) == 4); */
    /* assert(test_signed(INT_MAX, INT_MAX) == 0x3FFFFFFF); */
    /* assert(test_signed(INT_MIN, 10) == -5); */
    /* assert(test_signed(INT_MIN, INT_MIN) == 0x40000000); */
    /* assert(test_signed(INT_MIN, INT_MAX) == 0xC0000000); */
    /* assert(test_signed(0xFFFFFFFF, 0xFFFFFFFF) == 0); */

    assert(test_unsigned(99, 10) == 0);
    assert(test_unsigned(INT_MAX, 10) == 4);
    assert(test_unsigned(INT_MAX, INT_MAX) == 0x3FFFFFFF);
    assert(test_unsigned(INT_MIN, 10) == 5);
    assert(test_unsigned(INT_MIN, INT_MIN) == 0x40000000);
    assert(test_unsigned(INT_MIN, INT_MAX) == 0x3FFFFFFF);
    assert(test_unsigned(0xFFFFFFFF, 0xFFFFFFFF) == 0xFFFFFFFE);

    return 0;
}
