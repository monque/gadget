#include "devutils.h"
#include <assert.h>
#include <limits.h>

/* Divide by power of 2. Assume 0 <= k < w-1 */
/* 题目只想实现对负数的处理，结果顺手把round-to-even搞定了 */
int divide_power2(int x, int k) {
    int i;

    i = x << (31 - k);
    i = !(i & 0x40000000) || (i == 0x40000000);

    /* 两种实现皆可 */
    /* i = x << (31 - k); */
    /* i = (i == 0xC0000000) || (i & 0x40000000 && i << 2); */

    return (x >> k) + !i;
}

int main() {
    assert(divide_power2(11, 1) == 6);
    assert(divide_power2(12, 1) == 6);
    assert(divide_power2(13, 1) == 6);
    assert(divide_power2(14, 1) == 7);
    assert(divide_power2(15, 1) == 8);
    assert(divide_power2(16, 1) == 8);
    assert(divide_power2(17, 1) == 8);
    assert(divide_power2(18, 1) == 9);
    assert(divide_power2(19, 1) == 10);

    assert(divide_power2(-13, 1) == -6);
    assert(divide_power2(-15, 1) == -8);
    assert(divide_power2(-17, 1) == -8);

    assert(divide_power2(11, 2) == 3);
    assert(divide_power2(12, 2) == 3);
    assert(divide_power2(13, 2) == 3);
    assert(divide_power2(14, 2) == 4);
    assert(divide_power2(15, 2) == 4);
    assert(divide_power2(16, 2) == 4);
    assert(divide_power2(17, 2) == 4);
    assert(divide_power2(18, 2) == 4);
    assert(divide_power2(19, 2) == 5);

    return 0;
}
