#include "devutils.h"
#include <assert.h>
#include <limits.h>

/* Divide by power of 2. Assume 0 <= k < w-1 */
int divide_power2(int x, int k) {
    (x & INT_MIN) && (x = x + (1 << k) - 1);
    return x >> k;
}

int main() {
    assert(divide_power2(11, 1) == 5);
    assert(divide_power2(12, 1) == 6);
    assert(divide_power2(13, 1) == 6);

    assert(divide_power2(-13, 1) == -6);
    assert(divide_power2(-15, 1) == -7);
    assert(divide_power2(-17, 1) == -8);

    assert(divide_power2(11, 2) == 2);
    assert(divide_power2(12, 2) == 3);
    assert(divide_power2(13, 2) == 3);

    assert(divide_power2(-13, 2) == -3);
    assert(divide_power2(-15, 2) == -3);
    assert(divide_power2(-17, 2) == -4);

    return 0;
}
