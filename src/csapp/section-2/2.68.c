#include "devutils.h"
#include <assert.h>

/* Mask with least significant n bits set to 1
 * Examples: n = 6 --> 0x3F, n=17 --> 0x1FFFF
 * Assume 1 <= n <= w */
int lower_one_mask(int n) {
    int x;

    x = 1 << (n >> 1);
    x = x << (n - (n >> 1));
    x -= 1;

    return x;
}

int main() {
    int x;

    for (x = 0; x <= 32; x++) {
        showBits(lower_one_mask(x));
    }

    x = lower_one_mask(6);
    showBits(x);
    assert(x == 0x3F);

    x = lower_one_mask(17);
    showBits(x);
    assert(x == 0x1FFFF);

    x = lower_one_mask(32);
    showBits(x);
    assert(x == 0xFFFFFFFF);

    x = lower_one_mask(0);
    showBits(x);
    assert(x == 0x0);
}
