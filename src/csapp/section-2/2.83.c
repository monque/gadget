#include "devutils.h"
#include <assert.h>
#include <limits.h>

int main() {
    /* A */
    Y = B2U(y, k); /* y的位表示，按照unsigned进行转换 */
    y * 2^k - Y = y;
    y = Y / (2^k - 1);

    /* B */
    y = 0b101;
    num = 5 / 7;
    num = 0.714285714;

    y = 0b0110;
    num = 6 / 15;
    num = 0.4;

    y = 0b010011;
    num = 19 / 63;
    num = 0.301587302;
}
