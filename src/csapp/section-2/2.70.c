#include "devutils.h"
#include <assert.h>

/* Copy from 2.68 */
int lower_one_mask(int n) {
    int x;

    x = 1 << (n >> 1);
    x = x << (n - (n >> 1));
    x -= 1;

    return x;
}

/* Return 1 when x can be represented as an n-bit, 2's-complement
 * number; 0 otherwise. Assume 1 <= n <= w */
int fits_bits(int x, int n) {
    int m;

    m = ~lower_one_mask(n);

    showBits(x);
    showBits(m);

    return !(x & m);
}

int main() {
    assert(fits_bits(0xFF, 8) == 1);
    assert(fits_bits(0xFF, 7) == 0);

    assert(fits_bits(0xFFFFFFFF, 8) == 0);
    assert(fits_bits(0xFFFFFFFF, 31) == 0);
    assert(fits_bits(0xFFFFFFFF, 32) == 1);

    assert(fits_bits(0x00, 0) == 1);
}
