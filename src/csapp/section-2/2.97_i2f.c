#include "devutils.h"
#include "csapp.h"

/* Compute (float) i */
float_bits float_i2f(int i) {
    unsigned sign, frac, E, mask, r;

    if (i == 0) {
        return 0x00000000;
    }

    sign = i >> 31 & 0x1;
    frac = sign ? -i : i;

    /* Find left-most bit */
    for (mask = 0xFFFFFFFE, E = 0; frac & mask; mask <<= 1, E++) ;

    if (E > 23) {
        /* Round-to-even */
        r = frac << (31 - (E - 23));
        r = (r != 0x40000000) && (r & 0x40000000);
        frac = (frac >> (E - 23)) + r;

        /* frac overflow */
        E += (frac == 0x1000000);
    } else {
        frac <<= (23 - E);
    }

    return sign << 31 | (E + 127) << 23 | (frac & 0x7FFFFF);
}

int main() {
    int i;

    for (i = INT_MIN; i != INT_MAX; i++) {
        assert(float_i2f(i) == to_bits((float) i));
    }
}
