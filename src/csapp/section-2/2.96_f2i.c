#include "devutils.h"
#include "csapp.h"

/* Compute (int) f. If conversion causes overflow or f is NaN, return
 * 0x80000000 */
int float_f2i(float_bits f) {
    unsigned sign, frac, exp, r;

    sign = f >> 31;
    exp = f >> 23 & 0xFF;
    frac = f & 0x7FFFFF;

    if (exp > 127 + 30) {
        /* E > 30, Overflow, NaN, Inf */
        return 0x80000000;
    } else if (exp < 127 - 1) {
        /* E < -1, Zero, Denormalized */
        return 0x0;
    }

    frac = frac | 0x800000;
    if (exp > 127 + 23) {
        frac <<= (exp - 127 - 23);
    } else if (exp < 127 + 23) {
        /* Round-to-zero */
        frac >>= 127 + 23 - exp;
    }

    return sign ? -frac : frac;
}

void test(float f) {
    int i, r;

    i = (int) f;
    r = float_f2i(to_bits(f));

    if (i != r) {
        printf("%f\t%d\t%d\n", f, i, r);
        assert(float_f2i(to_bits(f)) == (int) f);
    }
}

int main() {
    int i;
    float f;

    /* Special */
    test(INT_MIN);
    test(INT_MAX);
    test(0.0);
    test(-0.0);
    test(1.0 / 0.0);
    test(-1.0 / 0.0);
    test(1.0/0.0 - 1.0/0.0); /* NaN */

    /* Rounding */
    test(4.001); test(4.499); test(4.500); test(4.501); test(4.999);
    test(5.001); test(5.499); test(5.500); test(5.501); test(5.999);
    test(-4.001); test(-4.499); test(-4.500); test(-4.501); test(-4.999);
    test(-5.001); test(-5.499); test(-5.500); test(-5.501); test(-5.999);

    /* Normal Integer */
    for (i = -100; i <= 100; i++) {
        test((float) i);
    }

    /* Powers number */
    for (i = -150; i < 150; i++) {
        test(fpwr2(i));
        test(fpwr2(i) + fpwr2(i + 1));
        test(fpwr2(i) + fpwr2(i - 1));
        test(fpwr2(i) + fpwr2(1 - i));

        test(fpwr2(i) - fpwr2(i + 1));
        test(fpwr2(i) - fpwr2(i - 1));
        test(fpwr2(i) - fpwr2(1 - i));
    }
}
