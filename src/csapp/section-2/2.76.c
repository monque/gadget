#include "devutils.h"
#include <assert.h>
#include <limits.h>

#include <stdlib.h>
#include <string.h>

void *calloc(size_t nmemb, size_t size) {
    void *p;
    size_t total;

    if (nmemb == 0 || size == 0) {
        return NULL;
    }

    total = nmemb * size;
    if (nmemb != total / size) {
        return NULL;
    }

    p = malloc(nmemb * size);
    memset(p, 0, nmemb * size);
    return p;
}

int main() {
    int *p;
    
    p = calloc(10, 4);
    printf("0x%lX\n", p);

    return 0;
}
