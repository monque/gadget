#include "devutils.h"
#include <assert.h>

/* Do rotating left shift. Assume 0 <= n < w
 * Examples when x = 0x12345678 and w = 32:
 * n =  4 -> 0x23456781
 * n = 20 -> 0x67812345 */
unsigned rotate_left(unsigned x, int n) {
    unsigned o;

    o = x << n;
    o |= x >> (32 - n);

    return o;
}

int main() {
    unsigned o, x = 0x12345678;

    o = rotate_left(x, 4);
    printf("0x%X\n", o);
    assert(o == 0x23456781);

    o = rotate_left(x, 20);
    printf("0x%X\n", o);
    assert(o == 0x67812345);

    o = rotate_left(x, 0);
    printf("0x%X\n", o);
    assert(o == 0x12345678);
}
