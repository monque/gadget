#include "devutils.h"
#include <assert.h>
#include <limits.h>

int float_le(float x, float y) {
    unsigned ux = f2u(x);
    unsigned uy = f2u(y);

    /* Get the sign bits */
    unsigned sx = ux >> 31;
    unsigned sy = uy >> 31;

    /* Give an expression using only ux, uy, sx, and sy */
    if (sx == 0) {
        if (sy == 0) {
            return ux <= uy;
        } else {
            return ux == 0 && uy == 0x80000000;
        }
    } else {
        if (sy == 0) {
            return 1;
        } else {
            return ux >= uy;
        }
    }

    return (sx == 0 && ((sy == 0 && ux <= uy) || (sy == 1 && ux == 0 && uy == 0x80000000))) ||
    (sx == 1 && ((sy == 0) || (sy == 1 && ux >= uy)));
}

int main() {
}
