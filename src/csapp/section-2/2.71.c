#include "devutils.h"
#include <assert.h>

/* Declaration of data type where 4 bytes are packed into an unsigned */
typedef unsigned packed_t;

/* Extract byte from word. Return as signed integer */
int xbyte(packed_t word, int bytenum) {
    int x;

    x = word << (24 - (bytenum << 3));
    x >>= 24;

    return x;
}

/* Failed attempt at xbyte */
int xbyte_wrong(packed_t word, int bytenum) {
    return (word >> (bytenum << 3)) & 0xFF;
}

int main() {
    assert(xbyte(0x12345678, 0) == 0x78);
    assert(xbyte(0x12345678, 1) == 0x56);
    assert(xbyte(0x12345678, 2) == 0x34);
    assert(xbyte(0x12345678, 3) == 0x12);
    printf("0x%X\n", xbyte(0x12345678, 0));
    printf("0x%X\n", xbyte(0x12345678, 1));
    printf("0x%X\n", xbyte(0x12345678, 2));
    printf("0x%X\n", xbyte(0x12345678, 3));

    assert(xbyte(0xFFFFFFFF, 0) == 0xFFFFFFFF);
    assert(xbyte(0xFFFFFFFF, 1) == 0xFFFFFFFF);
    assert(xbyte(0xFFFFFFFF, 2) == 0xFFFFFFFF);
    assert(xbyte(0xFFFFFFFF, 3) == 0xFFFFFFFF);
    printf("0x%X\n", xbyte(0xFFFFFFFF, 0));
    printf("0x%X\n", xbyte(0xFFFFFFFF, 1));
    printf("0x%X\n", xbyte(0xFFFFFFFF, 2));
    printf("0x%X\n", xbyte(0xFFFFFFFF, 3));

    assert(xbyte(0x00000000, 0) == 0x0);
    assert(xbyte(0x00000000, 1) == 0x0);
    assert(xbyte(0x00000000, 2) == 0x0);
    assert(xbyte(0x00000000, 3) == 0x0);
    printf("0x%X\n", xbyte(0x00000000, 0));
    printf("0x%X\n", xbyte(0x00000000, 1));
    printf("0x%X\n", xbyte(0x00000000, 2));
    printf("0x%X\n", xbyte(0x00000000, 3));
}
