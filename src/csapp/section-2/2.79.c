#include "devutils.h"
#include <assert.h>
#include <limits.h>

/* Calculate (x * 3 / 4) */
int mul3div4(int x) {
    x = (x << 1) + x;
    (x & INT_MIN) && (x = x + 3); /* copy from divide_power2 */
    return x >> 2;
}

int main() {
    int i;

    for (i = -10000; i < 10000; i++) {
        /* printf("%d: %d %d\n", i, mul3div4(i), (3 * i / 4)); */
        assert(mul3div4(i) == (3 * i / 4));
    }

    /* Overflow */
    assert(mul3div4(INT_MAX) == (3 * INT_MAX / 4));
    assert(mul3div4(INT_MIN) == (3 * INT_MIN / 4));

    return 0;
}
