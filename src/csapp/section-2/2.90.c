#include "devutils.h"
#include <assert.h>
#include <limits.h>

float u2f(unsigned u) {
    return *(float *) &u;
}

float fpwr2_t(int x) {
    float f = 1;

    for (; x < 0; x++) f /= 2;
    for (; x > 0; x--) f *= 2;

    return f;
}

float fpwr2(int x) {
    /* Result exponent and fraction */
    unsigned exp, frac;
    unsigned u;

    if (x < -149) {
        /* Too small. Return 0.0 */
        exp = 0;
        frac = 0;
    } else if (x < -126) {
        /* Denormalized result */
        exp = 0;
        frac = 1 << (23 + x + 126);
    } else if (x < 128) {
        /* Normalized result */
        exp = x + 127;
        frac = 0;
    } else {
        /* Too big. Return +inf */
        exp = 255;
        frac = 0;
    }

    /* Pack exp and frac into 32 bits */
    u = exp << 23 | frac;

    return u2f(u);
}

int main() {
    int i, j;

    assert(fpwr2(0) == fpwr2_t(0));

    for (i = -150; i < 150; i++) {
        if (fpwr2(i) != fpwr2_t(i)) {
            printf("%d: %f %f\n", i, fpwr2(i), fpwr2_t(i));
        }
    }
}
