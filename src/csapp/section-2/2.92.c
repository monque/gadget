#include "devutils.h"
#include "csapp.h"

/* Compute -f. If f is Nan, then return f. */
float_bits float_negate(float_bits f) {
    unsigned sign, frac, exp;

    sign = f >> 31;
    exp = f >> 23 & 0xFF;
    frac = f & 0x7FFFFF;

    if (exp == 0xFF && frac != 0) {
        return f;
    }

    return sign ? f & 0x7FFFFFFF : f | 0x80000000;
}

void test(float f) {
    assert(to_bits(-f) == float_negate(to_bits(f)));
}

void test_nan(float f) {
    assert(to_bits(f) == float_negate(to_bits(f)));
}

int main() {
    float f;

    test_nan(1.0/0.0 - 1.0/0.0); /* NaN */
    test(98.76543);
    test(-9.0);
    test(0.0);
    test(-0.0);
    test(1.0 / 0.0);
    test(-1.0 / 0.0);
}
