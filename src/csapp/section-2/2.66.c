#include "devutils.h"
#include <assert.h>

/* Generate mask indicating leftmost 1 in x. Assume w = 32.
 * For example, 0xFF00 -> 0x8000, and 0x6600 --> 0x4000.
 * If x = 0, then return 0. */
int leftmost_one(unsigned x) {
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;

    return (x >> 1) + 1 & x;
}

int leftmost_one2(unsigned x) {
    unsigned e;
    double d;
    unsigned long *p;

    d = x;

    p = (unsigned long *) &d;

    e = (*p >> 52 & (1 << 11) - 1) - 1023;

    e = 1 << e;

    return e;
}

int main() {
    int i, o;

    for (i = 1; i < 65535; i++) {
        assert(leftmost_one(i) == leftmost_one2(i));
    }

    i = 0xFF00;
    o = leftmost_one(i);
    showBits(i);
    showBits(o);
    assert(o == 0x8000);

    i = 0x6600;
    o = leftmost_one(i);
    showBits(i);
    showBits(o);
    assert(o == 0x4000);
}
