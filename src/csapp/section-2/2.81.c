#include "devutils.h"
#include <assert.h>
#include <limits.h>

int a(int k) {
    return -1 << k;
}

int b(int k, int j) {
    return ~a(k) << j;
}

int main() {
    int i;

    for (i = 0; i <= 32; i++) {
        showBits(a(i));
    }

    for (i = 0; i <= 24; i++) {
        showBits(b(8, i));
    }
}
