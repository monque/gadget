#include "devutils.h"
#include "csapp.h"

/* Compute 2*f. If f is Nan, then return f. */
float_bits float_twice(float_bits f) {
    unsigned sign, frac, exp;

    sign = f >> 31;
    exp = f >> 23 & 0xFF;
    frac = f & 0x7FFFFF;

    if (exp == 0xFF && frac != 0) {
        return f;
    }

    if (exp == 0) {
        /* Denormalized */
        /* 当frac最高位为1时，该浮点数转为规格化数
         * 此时直接将frac左移，最高位会成为exp的最低位，即完成该转换 */
        frac <<= 1;
    } else if (exp < 254) {
        /* Normalized */
        exp += 1;
    } else if (exp == 254) {
        /* Too big. Return +inf */
        exp = 255;
        frac = 0;
    }

    return (sign << 31) | (exp << 23) | frac;
}

void test(float f) {
    assert(to_bits(f * 2) == float_twice(to_bits(f)));
}

void test_nan(float f) {
    assert(to_bits(f) == float_twice(to_bits(f)));
}

int main() {
    int i;
    float f;

    /* Special */
    test_nan(1.0/0.0 - 1.0/0.0); /* NaN */
    test(98.76543);
    test(-9.0);
    test(0.0);
    test(-0.0);
    test(1.0 / 0.0);
    test(-1.0 / 0.0);

    /* Powers number */
    for (i = -150; i < 150; i++) {
        test(fpwr2(i));
        test(fpwr2(i) + fpwr2(i + 1));
        test(fpwr2(i) + fpwr2(i - 1));
        test(fpwr2(i) + fpwr2(1 - i));

        test(fpwr2(i) - fpwr2(i + 1));
        test(fpwr2(i) - fpwr2(i - 1));
        test(fpwr2(i) - fpwr2(1 - i));
    }
}
