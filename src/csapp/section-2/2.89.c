#include "devutils.h"
#include <assert.h>
#include <limits.h>

void find_wrong_num() {
    short x, y, z;
    float fx, fy, fz, s1, s2;

    for (x = SHRT_MAX; x >= SCHAR_MIN; x--) {
        for (y = SHRT_MAX; y >= SCHAR_MIN; y--) {
            for (z = SCHAR_MAX; z >= SCHAR_MIN; z--) {
                fx = (float) x;
                fy = (float) y;
                fz = (float) z;
                s1 = (fx * fy) * fz;
                s2 = fx * (fy * fz);
                if (s1 != s2) {
                    printf("x  = %d\n", x);
                    printf("y  = %d\n", y);
                    printf("z  = %d\n", z);
                    printf("s1 = %f\n", s1);
                    printf("s2 = %f\n", s2);
                    printf("\n");
                    return;
                }
            }
        }
    }

    printf("Not found\n");
}

int main() {
    find_wrong_num();

    /* Create some arbitrary values */
    int x = random();
    int y = random();
    int z = random();


    /* Convert to double */
    double dx = (double) x;
    double dy = (double) y;
    double dz = (double) z;

    /* A right */
    (float) x == (float) dx;

    /* B */
    dx - dy == (double) (x - y);
    x = 1;
    y = -INT_MAX;
    dx = (double) x;
    dy = (double) y;
    printf("%lf\n", dx - dy);
    printf("%d\n", (x - y));
    printf("%lf\n", (double) (x - y));

    /* C right */
    /* 三个变量都是由int转为double
     * double拥有52位尾数位，完全可容纳int的31位
     * 故不会因变量交换导致溢出影响准确性*/
    dx + dy + dz == dz + dy + dx;

    /* D */
    /* 很有迷惑性。。。
     * 由于可能发生溢出，或者由于舍入儿失去精度，浮点数运算不具有结合性 P77
     * 通过构造，迫使乘法结果进行舍入，可使该等式不成立 */
    (dx * dy) * dz == dx * (dy * dz);
    x = 0x64e73387;
    y = 0xd31cb264;
    z = 0xd22f1fcd;
    dx = (double) x;
    dy = (double) y;
    dz = (double) z;

    showBits((dx * dy) * dz);
    showBits(dx * (dy * dz));

    /* E */
    dx / dx == dz / dz;
    dx = 0.0; /* NaN */
}
