#include "devutils.h"

unsigned int replace_byte (unsigned int x, int i, unsigned char b)
{
    unsigned int v;

    v = 0xFFFFFF00 << (i * 8) | ((1 << (i * 8)) - 1);
    x &= v;

    v = b << (i * 8);
    x |= v;

    return x;
}

int main()
{
    unsigned int v;

    v = replace_byte(0x12345678, 2, 0xAB);
    printf("0x%X\n", v);
    showBits(v);

    v = replace_byte(0x12345678, 0, 0xAB);
    printf("0x%X\n", v);
    showBits(v);
}
