#include "devutils.h"
#include <assert.h>
#include <limits.h>

int main() {
    /* Create some arbitrary values */
    int x = random();
    int y = random();

    /* Convert to unsigned */
    unsigned ux = (unsigned) x;
    unsigned uy = (unsigned) y;

    /* A. wrong */
    (x < y) == (-x > -y);
    x = INT_MIN;
    y = INT_MIN + 1;

    /* B. right */
    /* 本质为证明 (x + y) << 4 == (x << 4) + (y << 4) */
    ((x + y) << 4) + y - x == 17 * y + 15 * x;

    /* C. right */
    /* 根据补码非的表达式 ~x = -x - 1 （见第二版P60）
     * 带入后该表达式可转换为
     * -x - y -1 == -(x + y) - 1
     * 该表达式始终为真 */
    ~x + ~y + 1 == ~(x + y);

    /* D. right */
    /* 补码加法与无符号之和有完全相同的位级表示 */
    (ux - uy) == -(unsigned) (y - x);
    -(uy - ux) == -(unsigned) (y - x);

    /* E. right */
    /* 左边的操作是抹掉数字的最右两位，最右两位无论如何都表示正数，该等式为真 */
    ((x >> 2) << 2) <= x;
    (x & 0xFFFFFFFC) <= x;
}
