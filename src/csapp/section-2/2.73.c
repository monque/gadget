#include "devutils.h"
#include <assert.h>
#include <limits.h>

#define saturating_add saturating_add_bits
#define MASK_BY_BOOL(cond) (((cond) && 1) << 31 >> 31)

/* Addition that saturates to TMin or TMax */
int saturating_add_if(int x, int y) {
    int sum = x + y;

    if (x > 0 && y > 0 && sum < 0) {
        return INT_MAX;
    } else if (x < 0 && y < 0 && sum > 0) {
        return INT_MIN;
    } else {
        return sum;
    }
}

/* Addition that saturates to TMin or TMax */
int saturating_add_bits(int x, int y) {
    int sum, over_pos, over_neg, a, b, c;

    sum = x + y;

    /* (x > 0 && y > 0 && sum < 0) */
    over_pos = !(x & INT_MIN) && !(y & INT_MIN) && (sum & INT_MIN);
    /* (x < 0 && y < 0 && sum > 0) */
    over_neg = (x & INT_MIN) && (y & INT_MIN) && !(sum & INT_MIN);

    /* 可以将任意结果定义为 (sum | a) - d
     * 其中d又可以定义为    b + c
     * Positive Overflow    (sum | INT_MAX) - (INT_MIN)
     * Negative Overflow    (sum | INT_MAX) - (INT_MIN + INT_MAX)
     * Normal               (sum |       0) - (0) */
    a = INT_MAX & ((over_pos || over_neg) << 31 >> 31);
    b = INT_MIN & ((over_pos || over_neg) << 31 >> 31);
    c = INT_MAX & (over_neg << 31 >> 31);

    return (sum | a) - (b + c);
}

int main() {
    int x, y;

    assert(saturating_add(9, 9) == 18);
    assert(saturating_add(INT_MAX, 9) == INT_MAX);
    assert(saturating_add(INT_MAX - 10, 99) == INT_MAX);
    assert(saturating_add(INT_MIN, -9) == INT_MIN);
    assert(saturating_add(INT_MIN + 10, -99) == INT_MIN);

    for (x = 0; x < 100; x++) {
        for (y = 0; y < 100; y++) {
            assert((x + y) == saturating_add(x, y));
        }
    }
}
