#include "devutils.h"

int main()
{
    int x, y, s;

    x = 0x89ABCDEF;
    y = 0x76543210;

    s = (x & 0x000000FF) | (y & 0xFFFFFF00);

    printf("0x%X\n", s);
}
