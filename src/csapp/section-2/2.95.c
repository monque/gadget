#include "devutils.h"
#include "csapp.h"

/* Compute 0.5*f. If f is Nan, then return f. */
float_bits float_half(float_bits f) {
    unsigned sign, frac, exp, round_add;

    sign = f >> 31;
    exp = f >> 23 & 0xFF;
    frac = f & 0x7FFFFF;

    if (exp == 0xFF && frac != 0) {
        return f;
    }

    /* Round-to-even */
    round_add = (frac & 0x3) == 0x3;

    if (exp == 0) {
        /* Denormalized */
        frac >>= 1;
        frac += round_add;
    } else if (exp == 1) {
        /* Denormalized */
        exp = 0;
        frac = frac >> 1 | 0x400000;
        frac += round_add;
    } else if (exp <= 254) {
        /* Normalized */
        exp -= 1;
    }

    return (sign << 31) | (exp << 23) | frac;
}

void test(float f) {
    assert(to_bits(0.5 * f) == float_half(to_bits(f)));
}

void test_nan(float f) {
    assert(to_bits(f) == float_half(to_bits(f)));
}

int main() {
    int i;
    float f;

    /* Special */
    test_nan(1.0/0.0 - 1.0/0.0); /* NaN */
    test(98.76543);
    test(-9.0);
    test(0.0);
    test(-0.0);
    test(1.0 / 0.0);
    test(-1.0 / 0.0);

    /* Rounding */
    test(4.001); test(4.499); test(4.500); test(4.501); test(4.999);
    test(5.001); test(5.499); test(5.500); test(5.501); test(5.999);
    test(-4.001); test(-4.499); test(-4.500); test(-4.501); test(-4.999);
    test(-5.001); test(-5.499); test(-5.500); test(-5.501); test(-5.999);

    /* Powers number */
    for (i = -150; i < 150; i++) {
        test(fpwr2(i));
        test(fpwr2(i) + fpwr2(i + 1));
        test(fpwr2(i) + fpwr2(i - 1));
        test(fpwr2(i) + fpwr2(1 - i));

        test(fpwr2(i) - fpwr2(i + 1));
        test(fpwr2(i) - fpwr2(i - 1));
        test(fpwr2(i) - fpwr2(1 - i));
    }
}
