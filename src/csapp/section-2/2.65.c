#include "devutils.h"

/* Return 1 when x contains an odd number of 1s; 0 otherwise. Assume w = 32 */
int odd_ones(unsigned x) {
    /* 可以假设int有w = 32位 */
    /* 代码最多只能包含12个算数运算、位运算、逻辑运算 */
    x ^= (x >> 16);
    x ^= (x >> 8);
    x ^= (x >> 4);
    x ^= (x >> 2);
    x ^= (x >> 1);

    return x & 0x1;
}

int main() {
    int i;

    for (i = 0; i < 16; i++) {
        showBits(i);
        printf("%d : %d\n", i, odd_ones(i));
    }
}
