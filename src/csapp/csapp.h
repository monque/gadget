#include <assert.h>
#include <limits.h>

/* Access bit-level representation floating-point number */
typedef unsigned float_bits;

static float_bits to_bits(float f) {
    return *(float_bits *) &f;
}

static float to_float(float_bits b) {
    return *(float *) &b;
}

static float fpwr2(int x) {
    float f = 1;
    for (; x < 0; x++) f /= 2;
    for (; x > 0; x--) f *= 2;
    return f;
}
