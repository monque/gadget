#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define ONE_MBYTES 1048576
#define SLEEP_FOREVER for (;;) { sleep(1); }

void create_fd(void)
{
    long i;

    for (i = 0; ; i++) {
        if (socket(AF_UNIX, SOCK_STREAM, 0) == -1) {
            printf("socket created failed, after %ld\n", i);
            if (i == 0) {
                /* return if no one socket created */
                return;
            } else {
                break;
            }
        }
    }

    printf("%ld sockets created, go to sleep...\n", i);

    SLEEP_FOREVER
}

void flood_mem(void)
{
    long i, j, *p;

    for (i = 0; i < 1024; i++) {
        p = malloc(ONE_MBYTES);
        if (p == NULL) {
            printf("malloc failed, after %ld Mbytes\n", i);
            if (i == 0) {
                return;
            } else {
                break;
            }
        }
        for (j = 0; j < ONE_MBYTES / sizeof(*p); j++) {
            *(p + j) = j * i;
        }
    }

    printf("malloced: %ld Mbytes\n", i);

    SLEEP_FOREVER
}

int main(int argc, char *argv[])
{
    pid_t pid;
    void (*child_work)(void);

    child_work = NULL;
    if (argc < 2) {
    } else if (strcmp("fd", argv[1]) == 0) {
        child_work = create_fd;
    } else if (strcmp("mem", argv[1]) == 0) {
        child_work = flood_mem;
    }
    if (child_work == NULL) {
        printf("usage: bless (fd|mem)");
        return 0;
    }

    printf(
" ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ \n"
"                    _ooOoo_                    \n"
"                   o8888888o                   \n"
"                   88\" . \"88                   \n"
"                   (| -_- |)                   \n"
"                   O\\  =  /O                   \n"
"                ____/`---'\\____                \n"
"              .'  \\\\|     |//  `.              \n"
"             /  \\\\|||  :  |||//  \\             \n"
"            /  _||||| -:- |||||-  \\            \n"
"            |   | \\\\\\  -  /// |   |            \n"
"            | \\_|  ''\\---/''  |   |            \n"
"            \\  .-\\__  `-`  ___/-. /            \n"
"          ___`. .'  /--.--\\  `. . __           \n"
"       .\"\" '<  `.___\\_<|>_/___.'  >'\\\\"".        \n"
"      | | :  `- \\`.;`\\ _ /`;.`/ - ` : | |      \n"
"      \\  \\ `-.   \\_ __\\ /__ _/   .-` /  /      \n"
" ======`-.____`-.___\\_____/___.-`____.-'====== \n"
"                    `=---='                    \n"
" ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ \n"
    );
    for (;;) {
        pid = fork();
        if (pid == 0) { /* child */
            child_work();
            exit(EXIT_SUCCESS);
        } else if (pid > 0) { /* parent */
            sleep(1);
        } else if (pid == -1) {
            fprintf(stderr, "error: %d %s", errno, strerror(errno));
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}
