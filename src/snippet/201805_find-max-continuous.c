#include "devutils.h"
#include <time.h>

/* 昨天永宁骑车的时候突然想起TP求20min最大功率的算法，当时边骑边想没想出太好的
 * 办法（想复杂了）。回家对着电脑敲了10分钟搞定，其实就是复杂了一点点的找最大数
 * 见find_max_sub_n 201805 */

/* 好未来面试被问到类似的算法，恰好是之前想复杂了的问题解决方案，面试时没想出太
 * 好的解法，经过提示想到靠谱办法，顺手实现一下加强认识。20180614 */

/* Make Random Array */
void makeArr(int *arr, size_t size, int start, int step, char random) {
    int i, j, v;

    for (v = start, i = 0; i < size; i += 1, v += step) {
        arr[i] = v;
    }

    if (random) {
        while (--i) {
            j = rand() % (i + 1);

            v = arr[i];
            arr[i] = arr[j];
            arr[j] = v;
        }
    }
}

struct Sub {
    int sum;
    int head;
    int tail;
};

/* 在数组中找到最大的连续n个数 */
struct Sub find_max_sub_n(int *arr, int size, int n) {
    int i, sum;
    struct Sub sub;

    for (sum = 0, i = 0; i < n; i++) {
        sum += arr[i];
    }

    for (sub.sum = sum, sub.head = 0, sub.tail = n - 1; i < size; i++) {
        sum = sum + arr[i] - arr[i - n];
        if (sum > sub.sum) {
            sub.sum = sum;
            sub.head = i - n + 1;
            sub.tail = i;
        }
    }

    /* printf("Max sub-%d: %d from %d to %d\n", n, sub.sum, sub.head, sub.tail); */

    return sub;
}

struct Sub find_max_sub2(int *arr, int size) {
    int i;
    struct Sub sub, max;

    max = find_max_sub_n(arr, size, 1);
    for (i = 2; i < size; i++) {
        sub = find_max_sub_n(arr, size, i);
        if (sub.sum > max.sum) {
            max = sub;
        }
    }

    return max;
}

struct Sub find_max_sub(int *arr, int size) {
    int i, sum, head;
    struct Sub sub;

    sub.sum = arr[0];
    sub.head = 0;
    for (head = 0, sum = 0, i = 0; i< size; i++) {
        sum += arr[i];
        if (sum > sub.sum) {
            sub.sum = sum;
            sub.head = head;
            sub.tail = i;
        }
        if (sum < 0) {
            sum = 0;
            head = i + 1;
        }
    }

    return sub;
}

int main() {
    int i, arrsiz, *arr;
    struct Sub sub1, sub2;

    arrsiz = 100;
    arr = calloc(arrsiz, sizeof(int));
    srand((unsigned) time(NULL));

    for (i = 0; i < 1000000; i++) {
        makeArr(arr, arrsiz, -arrsiz / 2, 1, 1);

        sub1 = find_max_sub(arr, arrsiz);

        sub2 = find_max_sub2(arr, arrsiz);

        if (sub1.sum != sub2.sum) {
            printf("Max sub1: %d from %d to %d\n", sub1.sum, sub1.head, sub1.tail);
            printf("Max sub2: %d from %d to %d\n", sub2.sum, sub2.head, sub2.tail);
            break;
        }
    }
}
