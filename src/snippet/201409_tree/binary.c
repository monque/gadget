#include <stdio.h>
#include <stdlib.h>

/* Binary Tree */

// Const
enum direction {
    OLYLEFT,
    OLYRIGHT
};

// Struct
typedef struct binary_node {
    struct binary_node *left;
    struct binary_node *right;
    int key;
} BinaryNode;

typedef struct binary_tree {
    struct binary_node *root;
    unsigned quantity;
} BinaryTree;

// Function
BinaryTree *binary_init_tree() {
    BinaryTree *tree;

    tree = malloc(sizeof(BinaryTree));
    tree->root = NULL;
    tree->quantity = 0;

    return tree;
}

BinaryNode *binary_init_node(int key) {
    BinaryNode *node;

    node = malloc(sizeof(BinaryNode));
    node->left = node->right = NULL;
    node->key = key;

    return node;
}

BinaryNode **binary_search_ptr(BinaryTree *tree, int key) {
    BinaryNode **ptr_node;

    ptr_node = &tree->root;
    while (*ptr_node != NULL && (*ptr_node)->key != key) {
        if (key < (*ptr_node)->key) {
            ptr_node = &(*ptr_node)->left;
        } else {
            ptr_node = &(*ptr_node)->right;
        }
    }

    return ptr_node;
}

BinaryNode *binary_search(BinaryTree *tree, int key) {
    return *binary_search_ptr(tree, key);
}

unsigned binary_measure_height(BinaryNode *top, BinaryNode **ptr_bottom) {
    unsigned height_left, height_right;
    BinaryNode *bottom_left, *bottom_right;

    if (top == NULL) // Empty node
        return 0;
    else if (top->left == NULL && top->right == NULL) {// Leaf node
        ptr_bottom != NULL && (*ptr_bottom = top);
        return 1;
    }

    // Recursive
    height_left = height_right = 1;
    if (top->left != NULL)
        height_left += binary_measure_height(top->left, &bottom_left);
    if (top->right != NULL)
        height_right += binary_measure_height(top->right, &bottom_right);

    // Return max
    if (height_left > height_right) {
        ptr_bottom != NULL && (*ptr_bottom = bottom_left);
        return height_left;
    } else {
        ptr_bottom != NULL && (*ptr_bottom = bottom_right);
        return height_right;
    }
}

unsigned binary_measure_height_direction(BinaryNode *top, unsigned direction, BinaryNode **ptr_bottom) {
    unsigned height;

    if (top == NULL) // Empty node
        return 0;

    height = 1;
    *ptr_bottom = top;
    if (direction == OLYLEFT) {
        for (; (*ptr_bottom)->left != NULL; *ptr_bottom = (*ptr_bottom)->left, ++height)
            ;
    } else {
        for (; (*ptr_bottom)->right != NULL; *ptr_bottom = (*ptr_bottom)->right, ++height)
            ;
    }

    return height;
}

void binary_rotate_left(BinaryNode **ptr_root) {
    BinaryNode *pivot, *root;

    root = *ptr_root;
    pivot = root->right;

    root->right = pivot->left;
    pivot->left = root;
    *ptr_root = pivot;
}

void binary_rotate_right(BinaryNode **ptr_root) {
    BinaryNode *pivot, *root;

    root = *ptr_root;
    pivot = root->left;

    root->left = pivot->right;
    pivot->right = root;
    *ptr_root = pivot;
}

BinaryNode *binary_insert(BinaryTree *tree, int key) {
    BinaryNode **ptr_node;

    ptr_node = binary_search_ptr(tree, key);
    if (*ptr_node != NULL)
        return NULL;

    // Add new
    ++tree->quantity;
    return *ptr_node = binary_init_node(key);
}

int avl_factor(BinaryNode *node) {
    int factor;

    factor = 0;
    if (node->left != NULL) {
        factor += binary_measure_height(node->left, NULL);
    }
    if (node->right != NULL) {
        factor -= binary_measure_height(node->right, NULL);
    }

    return factor;
}

void avl_rebalance(BinaryNode **ptr_node) {
    int factor;

    factor = avl_factor(*ptr_node);

    if (factor >= 2) {
        if ((*ptr_node)->left && avl_factor((*ptr_node)->left) == -1) {
            binary_rotate_left(&(*ptr_node)->left);
        }
        binary_rotate_right(ptr_node);
    } else if (factor <= -2) {
        if ((*ptr_node)->right && avl_factor((*ptr_node)->right) == 1) {
            binary_rotate_right(&(*ptr_node)->right);
        }
        binary_rotate_left(ptr_node);
    }
}

#define avl_insert(tree, key) avl_insert_ex(tree, &tree->root, key)
BinaryNode *avl_insert_ex(BinaryTree *tree, BinaryNode **ptr_node, int key) {
    BinaryNode *new;

    if (*ptr_node == NULL) {  // Add new
        ++tree->quantity;
        return *ptr_node = binary_init_node(key);
    } else if (key == (*ptr_node)->key) {
        return NULL;
    } else if (key < (*ptr_node)->key) {
        new = avl_insert_ex(tree, &(*ptr_node)->left, key);
    } else {
        new = avl_insert_ex(tree, &(*ptr_node)->right, key);
    }

    avl_rebalance(ptr_node);

    return new;
}

void avl_remove(BinaryTree *tree, int key) {
    // TODO
}

void binary_remove(BinaryTree *tree, int key) {
    unsigned len_left, len_right;
    BinaryNode **ptr_removed, *removed, *bottom_left, *bottom_right;

    ptr_removed = binary_search_ptr(tree, key);
    removed = *ptr_removed;
    if (ptr_removed == NULL) {
        return;
    } else if (removed->left == NULL && removed->right == NULL) {
        *ptr_removed = NULL;
    } else if (removed->left == NULL) {
        *ptr_removed = removed->right;
    } else if (removed->right == NULL) {
        *ptr_removed = removed->left;
    } else {
        // Detect which direction is shorter
        len_left = binary_measure_height_direction(removed->left, OLYRIGHT, &bottom_right);  // Right-most-sub
        len_right = binary_measure_height_direction(removed->right, OLYLEFT, &bottom_left);  // Left-most-sub

        if (len_left < len_right) {
            *ptr_removed = removed->left;
            bottom_right->right = removed->right;
        } else {
            *ptr_removed = removed->right;
            bottom_left->left = removed->left;
        }
    }

    --tree->quantity;
    free(removed);
}

void binary_display_linear(BinaryTree *tree, BinaryNode *node) {
    if (tree == NULL || node == NULL)
        return;

    // Display header
    if (tree->root == node) {
        printf("-- Display linear --\n");
    }

    if (node->left != NULL)
        binary_display_linear(tree, node->left);

    printf("%d ", node->key);

    if (node->right != NULL)
        binary_display_linear(tree, node->right);

    // Display footer
    if (tree->root == node)
        putchar('\n');
}

void binary_display_diagram(BinaryTree *tree, BinaryNode *node, unsigned height) {
    if (tree == NULL || node == NULL)
        return;

    // Display header
    if (height == 0) {
        printf("-- Display diagram --\n");
        printf("Quantity: %d\n", tree->quantity);
        printf("Height: %u\n", binary_measure_height(tree->root, NULL));
    }

    if (node->left != NULL)
        binary_display_diagram(tree, node->left, height + 1);

    printf("%*d\n", height * 4, node->key);

    if (node->right != NULL)
        binary_display_diagram(tree, node->right, height + 1);

    // Display footer
}

/* main */
int main(int argc, char *argv[])
{
    BinaryTree *tree;
    BinaryNode *node;

    tree = binary_init_tree();

    { // Dev generate
        /* int i, arr[] = {5, 2, 8, 0, 4, 6, 9, 1, 3, 7}; */
        int i, arr[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        for (i = 0; i < sizeof(arr) / sizeof(int); ++i) {
            avl_insert(tree, arr[i]);
        }
    }

    binary_display_diagram(tree, tree->root, 0);
    binary_display_linear(tree, tree->root);

    if (0) { // Dev remove
        binary_remove(tree, 8);
        binary_display_diagram(tree, tree->root, 0);
    }

    if (0) { // Dev rotate
        binary_rotate_left(binary_search_ptr(tree, 0));
        binary_rotate_left(binary_search_ptr(tree, 2));
        binary_display_diagram(tree, tree->root, 0);
    }

    if (1) { // Dev avl
        avl_insert(tree, 10);
        binary_display_diagram(tree, tree->root, 0);
    }

    if (0) { // Dev measure height
        unsigned height;
        height = binary_measure_height(tree->root, &node);
        height = binary_measure_height_direction(tree->root, OLYRIGHT, &node);
        printf("Height: %u\n", height);
        printf("Bottom: %d\n", node->key);
    }
}
