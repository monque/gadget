#include <stdio.h>
#include <string.h>

int
isutf8(const char *str) // 检查是否为多字节utf8，返回该字符的字节数，错误返回-1
{
	unsigned char *p;
	unsigned int bytelen;

	p = (unsigned char *) str;
	if ((*p & 0x80) == 0)
		return 1; // single-byte

	/**/ if ((*p & 0xE0) == 0xC0)
		bytelen = 2;
	else if ((*p & 0xF0) == 0xE0)
		bytelen = 3;
	else if ((*p & 0xF8) == 0xF0)
		bytelen = 4;
	else if ((*p & 0xFC) == 0xF8)
		bytelen = 5;
	else if ((*p & 0xFE) == 0xFC)
		bytelen = 6;
	else
		return -1; // not a UTF-8 first byte

	for (p += bytelen - 1; p > (unsigned char *) str; p--)
		if ((*p & 0xC0) != 0x80)
			return -2; // not a UTF-8 tail byte

	return bytelen;
}

char *m_strrev(char *str)
{
	char *ps, *pe;

	ps = str;
	pe = str + strlen(str) - 1;
	for (; ps < pe; ps++, pe--)
		*ps ^= *pe, *pe ^= *ps, *ps ^= *pe;
	return str;
}

char *m_mb_strrev(char *str)
{
	char *ps, *pe, *pmb, swap;
	unsigned int bytelen;

	for (ps = str; *ps != '\0'; ps++) {
		if ((bytelen = isutf8(ps)) > 1) {
			for (pmb = pe = ps + bytelen - 1; ps < pe; ps++, pe--)
				swap = *ps, *ps = *pe, *pe = swap;
			ps = pmb;
		}
	}

	for (pe = ps - 1, ps = str; ps < pe; ps++, pe--)
		swap = *ps, *ps = *pe, *pe = swap;

	return str;
}

int main(void)
{
	char str[4096] = "360安全卫士", buf[4096];

	// Original
	printf(" original: %s\n", str);

	// Multi-byte strrev
	strcpy(buf, str);
	printf("mb_strrev: %s\n", m_mb_strrev(buf));

	// Strrev
	strcpy(buf, str);
	printf("   strrev: %s\n", m_strrev(buf));

	return 0;
}

