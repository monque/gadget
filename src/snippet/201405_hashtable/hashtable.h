typedef char *TableKey;
typedef void *TableVal;

typedef struct _TableSlot {
    TableKey key;
    TableVal val;
    struct _TableSlot *next;
} TableSlot;

typedef struct _TableEntry {
    unsigned size;
    TableSlot *slots;
} TableEntry;


TableEntry *table_init(unsigned size);

TableSlot *table_set(TableEntry *te, TableKey key, TableVal val);

TableSlot *table_get(TableEntry *te, TableKey key);

void table_destroy(TableEntry *te);
