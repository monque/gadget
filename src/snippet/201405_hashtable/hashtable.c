#include <stdlib.h>
#include <string.h>
#include "hashtable.h"

#define table_initslot(ts) ts->key = ts->val = ts->next = NULL

unsigned hash_val(TableKey key, unsigned size) {
	unsigned hashval;

	for (hashval = 0; *key != '\0'; key++)
		hashval = *key * 31 + hashval;

	return hashval % size;
}

TableEntry *table_init(unsigned size) {
    TableEntry *te;
    TableSlot *ts;

    te = malloc(sizeof(TableEntry));
    te->size = size;
    te->slots = malloc(sizeof(TableSlot) * size);
    for (ts = te->slots; ts < te->slots + size; ts++) {
        table_initslot(ts);
    }

    return te;
}

TableSlot *table_set(TableEntry *te, TableKey key, TableVal val) {
    TableSlot *ts;

    ts = te->slots + hash_val(key, te->size);
    while (ts->key && strcmp(key, ts->key) != 0) {
        if (!ts->next) {
            ts->next = malloc(sizeof(TableSlot));
            table_initslot(ts->next);
        }
        ts = ts->next;
    }

    ts->key = key;
    ts->val = val;

    return ts;
}

TableSlot *table_get(TableEntry *te, TableKey key) {
    TableSlot *ts;

    ts = te->slots + hash_val(key, te->size);
    do {
        if (ts->key && strcmp(key, ts->key) == 0) {
            return ts;
        }
    } while ((ts = ts->next));

    return ts;
}

void table_destroy(TableEntry *te) {
    free(te->slots);
    free(te);
}
