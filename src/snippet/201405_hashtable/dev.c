#include <stdio.h>
#include "hashtable.h"

#define debug_setget(te, k, v, ts)                              \
do {                                                            \
    table_set(te, k, (TableVal) v);                              \
    ts = table_get(te, k);                                       \
    if (ts) {                                           \
        printf("key (%s): %lu\n", k, (unsigned long) ts->val);  \
    } else {                                                    \
        printf("key (%s) not in table\n", k);                   \
    }                                                           \
} while (0)

#define debug_get(te, k, ts)                                    \
do {                                                            \
    ts = table_get(te, k);                                       \
    if (ts) {                                           \
        printf("key (%s): %lu\n", k, (unsigned long) ts->val);  \
    } else {                                                    \
        printf("key (%s) not in table\n", k);                   \
    }                                                           \
} while (0)


int main(int argc, char **argv) {
    TableEntry *te;
    TableSlot *ts;

    te = table_init(10);

    debug_setget(te, "fuck", 987, ts);
    debug_setget(te, "fuck", 1234, ts);
    debug_setget(te, "key_abc", 824, ts);
    debug_get(te, "fuck", ts);

    return 0;
}
