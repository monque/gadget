#! /usr/bin/env python
import random

CO_RESET = '\x1b[0m'
COLORS = [
    '\x1b[1;31m',
    '\x1b[1;32m',
    '\x1b[1;33m',
    '\x1b[1;34m',
    '\x1b[1;35m',
    '\x1b[1;36m',
    '\x1b[1;37m',
    '\x1b[1;30m',
]


class Board(object):

    def __init__(self, size=10, colornum=5):
        self.size = size
        self.colornum = colornum
        self.score = 0
        self.reset()

    # Utility
    def color(self, coord, color=False):
        x, y = coord
        if color is False:
            return self.matrix[x][y]
        else:
            self.matrix[x][y] = color

    def save(self):
        return [self.size, self.colornum, self.score, self.matrix]

    def load(self, data):
        self.size, self.colornum, self.score = data[:3]
        self.matrix = []
        for x in data[3]:
            self.matrix.append(x[:])

    def display(self, coord=None):
        print('=' * self.size * 2)
        print('Total: %d Click: %d' % (self.score, len(self.get_allregions())))
        for y in range(self.size):
            for x in range(self.size):
                color = self.color((x, y))
                if color is None:
                    print('.', end=' ')
                elif coord is not None and coord == (x, y):
                    print(COLORS[color] + 'O' + CO_RESET, end=' ')
                else:
                    print(COLORS[color] + 'x' + CO_RESET, end=' ')
            print('')
        print('-' * self.size * 2)

    # Base action
    def reset(self):
        self.matrix = [[None] * self.size for x in range(self.size)]
        self.score = 0

        for y in range(self.size):
            for x in range(self.size):
                self.color((x, y), random.randrange(self.colornum))

    def get_adjacent(self, coord):
        blocks = []

        color = self.color(coord)
        if color is None:
            return blocks

        news = []
        x, y = coord
        if y > 0:  # Up
            news.append((x, y - 1))
        if y < self.size - 1:  # Down
            news.append((x, y + 1))
        if x > 0:  # Left
            news.append((x - 1, y))
        if x < self.size - 1:  # Right
            news.append((x + 1, y))

        for new in news:
            if self.color(new) == color:
                blocks.append(new)

        return blocks

    def get_region(self, coord, allow_single=False):
        blocks = []

        color = self.color(coord)
        if color is None:
            return blocks

        stack = [coord]
        while stack:
            coord = stack.pop()
            if coord in blocks:
                continue

            blocks.append(coord)
            for new in self.get_adjacent(coord):
                stack.append(new)

        if not allow_single and len(blocks) == 1:
            return []
        else:
            blocks.sort()  # make region unique
            return blocks

    def get_allregions(self):
        blocks = []
        regions = []

        for y in range(0, self.size):
            for x in range(self.size):
                if (x, y) in blocks:
                    continue
                region = self.get_region((x, y))
                if not region:
                    continue
                blocks.extend(region)
                regions.append(region)

        return regions

    def remove(self, region):
        for coord in region:
            self.color(coord, None)

    def shift(self, line, size, is_empty, reverse):
        if reverse:
            line.reverse()
        for i in range(size - 1):
            # Find empty slot
            if not is_empty(line[i]):
                continue

            # Find next non-empty slot
            for j in range(i, size):
                if not is_empty(line[j]):
                    break
            if is_empty(line[j]):
                break

            # Swap it
            line[i], line[j] = line[j], line[i]
        if reverse:
            line.reverse()

    def move(self):

        def is_empty_block(block):
            return (block is None)

        def is_empty_line(line):
            return (line[self.size - 1] is None)

        for x in range(self.size):
            self.shift(self.matrix[x], self.size, is_empty_block, reverse=True)

        self.shift(self.matrix, self.size, is_empty_line, reverse=False)

    # User action
    def click(self, coord):
        region = self.get_region(coord)
        region_len = len(region)

        self.remove(region)
        self.move()
        self.score += pow(region_len, 2) * 5


def find_max_score(board):
    stack = [(board.save(), [])]
    results = []
    while stack:
        data, path = stack.pop()

        # Restore
        board.load(data)

        regions = board.get_allregions()

        # Record
        if not regions:
            results.append({
                'path': path,
                'score': board.score,
            })
            continue

        # Try all regions
        for region in regions:
            coord = region[0]
            board.click(coord)
            stack.append((board.save(), path + [coord]))
            board.load(data)

    return results


def display(board, path):
    for coord in path:
        board.display(coord)
        board.click(coord)
    board.display()


if __name__ == '__main__':
    data = [10, 5, 0, [
        [0, 0, 2, 1, 2, 1, 1, 4, 2, 0],
        [2, 2, 0, 2, 2, 3, 0, 0, 4, 0],
        [2, 2, 3, 3, 2, 2, 2, 2, 2, 2],
        [2, 2, 3, 1, 3, 4, 4, 1, 3, 3],
        [2, 3, 3, 4, 3, 0, 3, 4, 3, 0],
        [1, 0, 4, 1, 3, 1, 1, 4, 4, 1],
        [1, 4, 2, 3, 0, 3, 4, 2, 3, 1],
        [4, 4, 0, 2, 4, 1, 2, 0, 2, 2],
        [3, 1, 1, 4, 3, 2, 3, 0, 1, 2],
        [4, 1, 3, 2, 1, 3, 0, 1, 0, 0],
    ]]

    # Init
    board = Board(size=5, colornum=4)
    board.reset()
    # board.load(data)
    board.display()

    # Display
    # display(board, [(4, 5), (0, 3), (1, 5), (0, 3), (3, 2), (0, 3), (0, 4)])

    # Find
    results = find_max_score(board)

    # Output
    print('%d solutions been tried' % len(results))
    results.sort(key=lambda x: x['score'], reverse=True)
    for result in results[:10]:
        print('%d\t%s' % (result['score'], result['path']))
