#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define true    1
#define false   0
typedef unsigned char bool;
static struct timespec ti = {0, 100000000};

int swap_counter = 0;
int round_counter = 0;

#define showarrv(mark) do {     \
    showarr(arr, size, mark, NULL);   \
    nanosleep(&ti, NULL);       \
} while (0)

#define swap(l, r) do { \
    static int _swap; \
    swap_counter++; \
    _swap = arr[l];          \
    arr[l] = arr[r];              \
    arr[r] = _swap;          \
} while (0)

#define SORT_FUNC(method) void sort_##method(int arr[], size_t size)

#define SORT_RUN(method) do  {      \
    swap_counter = round_counter = 0; \
    printf("Sort %s\n", #method);   \
    memcpy(arr, ori, sizeof(int) * size); \
    showarr(arr, size, -1, "init");         \
    sort_##method(arr, size);       \
    showarr(arr, size, -1, "finish");         \
} while (0)

/* ******************** Array Function ******************** */
void makearr(int arr[], size_t size, bool random) {
    int i, j;

    // Init
    for (i = 0; i < size; i++) {
        arr[i] = i;
    }

    // Random
    if (random) {
        srand((unsigned) time(NULL));
        while (--i) {
            j = rand() % (i + 1);
            swap(i, j);
        }
    }
}

void showarr(int arr[], size_t size, int mark, char *title) {
	int x, y;

	printf("\ec");

    if (title == NULL) {
        printf("r %-3d s %-3d\n", ++round_counter, swap_counter);
    } else {
        printf("r %-3d s %-3d %s\n", round_counter, swap_counter, title);
    }

    for (x = 0; x < size * 2; x++)
        putchar('_');
    printf("__\n");

    for (y = size - 1; y >= 0; y--) {
        putchar('|');
        for (x = 0; x < size; x++) {
            if (arr[x] == y) {
                if (x == mark) {
                    printf(" #");
                } else {
                    printf(" *");
                }
            } else {
                printf("  ");
            }
        }
        printf("|\n");
    }

    for (x = 0; x < size * 2; x++)
        putchar('-');
    printf("--\n");
}

/* ******************** Sort Function ******************** */
SORT_FUNC(selection) {
    int i, j, min;

    for (i = 0; i < size - 1; i++) {
        min = i;
        for (j = i + 1; j < size; j++) {
            if (arr[j] < arr[min]) {
                min = j;
            }
        }
        swap(min, i);
        showarrv(-1);
    }
}

SORT_FUNC(bubble) {
    bool swapped;
    int i, j, min;

    for (i = 0; i < size; i++) {
        swapped = false;
        for (j = 0; j < size - 1 - i; j++) {
            if (arr[j] > arr[j + 1]) {
                swapped = true;
                swap(j, j + 1);
            }
        }
        if (!swapped) {
            break;
        }
        showarrv(-1);
    }
}

SORT_FUNC(cocktail) {
    bool swapped;
    int i;

    swapped = true;
    while (swapped) {
        swapped = false;

        for (i = 0; i < size - 1; i++) {
            if (arr[i + 1] < arr[i]) {
                swap(i + 1, i);
                swapped = true;
            }
        }
        showarrv(-1);

        for (; i > 0; i--) {
            if (arr[i] < arr[i - 1]) {
                swap(i, i - 1);
                swapped = true;
            }
        }
        showarrv(-1);
    }
}

void sort_quick_inner_a(int arr[], int left, int right, size_t size) {
    int pivot, store, i;

    if (left >= right)
        return;

    pivot = right;
    store = left;
    for (i = left; i < right; i++) {
        if (arr[i] < arr[pivot]) {
            swap(store, i);
            store++;
        }
    }
    swap(store, pivot);
    showarrv(store);

    sort_quick_inner_a(arr, left, store - 1, size);
    sort_quick_inner_a(arr, store + 1, right, size);
}

void sort_quick_inner_b(int arr[], int left, int right, size_t size) {
    int l, r, pivot, pv;

    if (left >= right)
        return;

    pivot = (left + right) / 2;
    pv = arr[pivot];
    for (l = left, r = right; l < r;) {
        for (; l < pivot && arr[l] <= pv; l++) {}
        if (l < pivot) {
            arr[pivot] = arr[l];
            pivot = l;
        }

        for (; r > pivot && arr[r] > pv; r--) {}
        if (r > pivot) {
            arr[pivot] = arr[r];
            pivot = r;
        }
    }
    arr[pivot] = pv;
    showarrv(-1);

    sort_quick_inner_b(arr, left, pivot - 1, size);
    sort_quick_inner_b(arr, pivot + 1, right, size);
}


SORT_FUNC(quick_a) {
    sort_quick_inner_a(arr, 0, size - 1, size);
}

SORT_FUNC(quick_b) {
    sort_quick_inner_b(arr, 0, size - 1, size);
}

SORT_FUNC(insertion) {
    int i, j;

    for (i = 1; i < size; i++) {
        for (j = i; j > 0 && arr[j] < arr[j - 1]; j--) {
            swap(j, j - 1);
            showarrv(j - 1);
        }

        /*
         * _swap = arr[i];
         * for (j = i; j > 0 && _swap < arr[j - 1]; j--) {
         *     arr[j] = arr[j - 1];
         * }
         * arr[j] = _swap;
         * showarrv(j);
         */
    }
}

SORT_FUNC(shell) {
    int i, j, gap;

    for (gap = size / 2; gap > 0; gap /= 2) {
        for (i = gap; i < size; i++) {
            for (j = i; j >= gap && arr[j] < arr[j - gap]; j -= gap) {
                swap(j, j - gap);
                showarrv(j - gap);
            }
        }
    }
}

void sort_merge_merge(int *arr_a, size_t size_a, int *arr_b, size_t size_b) {
    int *arr_merged, *pm, *pa, *pb;
    size_t mlen;

    mlen = sizeof(int) * (size_a + size_b);
    pm = arr_merged = (int *) malloc(mlen);

    for (pa = arr_a, pb = arr_b; pa < arr_a + size_a && pb < arr_b + size_b;) {
        if (*pa < *pb) {
            *(pm++) = *(pa++);
        } else {
            *(pm++) = *(pb++);
        }
    }

    while (pa != arr_a + size_a) {
        *(pm++) = *(pa++);
    }
    while (pb != arr_b + size_b) {
        *(pm++) = *(pb++);
    }

    memcpy(arr_a, arr_merged, mlen);
    free(arr_merged);
}

SORT_FUNC(merge) { /* int arr[], size_t size */
    int *arr_a, *arr_b;
    size_t size_a, size_b;

    if (size < 2) {
        return;
    }

    size_a = size / 2;
    size_b = size - size_a;
    arr_a = arr;
    arr_b = arr + size_a;

    sort_merge(arr_a, size_a);
    sort_merge(arr_b, size_b);

    sort_merge_merge(arr_a, size_a, arr_b, size_b);
}

void sort_heap_reheap(int *arr, int pivot, size_t size) {
    int child;

    while (1) {
        // Border
        child = pivot * 2 + 1;
        if (child >= size)
            break;

        // Find min child
        if (child + 1 < size && arr[child + 1] > arr[child])
            ++child;

        // Swap child, pivot
        if (arr[child] > arr[pivot]) {
            swap(child, pivot);
        } else {
            break;
        }

        pivot = child;
    }
}

SORT_FUNC(heap) {
    int i;

    // Adjust
    for (i = size / 2; i > 0; i--) {
        sort_heap_reheap(arr, i - 1, size);
    }

    // Sort
    for (i = size - 1; i > 0; i--) {
        swap(0, i);
        sort_heap_reheap(arr, 0, i);
        showarrv(-1);
    }
}

/* ****************************** Main ****************************** */
int main(int argc, char *argv[])
{
    bool devmode;
    int *arr, *ori;
    int arr_dev[] = { 5, 11,  4, 13,  6, 15, 19,  7, 14, 10,
                     18,  0, 12,  2, 17,  3,  1, 16,  8,  9};
    size_t size;

    if (argc != 3) {
        printf("usage: %s <method> <size>\n", argv[0]);
        return 1;
    }

    // Init
    size = (size_t) atoi(argv[2]);
    devmode = (size == 0);
    if (devmode) {
        size = sizeof(arr_dev) / sizeof(int);
    }
    ori = malloc(sizeof(int) * size * 2);
    arr = ori + size;
    makearr(ori, size, true);
    if (devmode) {
        memcpy(ori, arr_dev, sizeof(arr_dev));
    }

    // Sort
    if (strcmp(argv[1], "selection") == 0) {
        SORT_RUN(selection);
    } else if (strcmp(argv[1], "bubble") == 0) {
        SORT_RUN(bubble);
    } else if (strcmp(argv[1], "cocktail") == 0) {
        SORT_RUN(cocktail);
    } else if (strcmp(argv[1], "quick2") == 0) {
        SORT_RUN(quick_b);
    } else if (strncmp(argv[1], "quick", 5) == 0) { /* default quick sort */
        SORT_RUN(quick_a);
    } else if (strcmp(argv[1], "shell") == 0) {
        SORT_RUN(shell);
    } else if (strcmp(argv[1], "insertion") == 0) {
        SORT_RUN(insertion);
    } else if (strcmp(argv[1], "merge") == 0) {
        SORT_RUN(merge);
    } else if (strcmp(argv[1], "heap") == 0) {
        SORT_RUN(heap);
    } else {
        printf("error: invalid method\n");
        return 1;
    }

	return 0;
}
