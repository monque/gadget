# coding: UTF-8
from queue import PriorityQueue
from random import randrange


"""
Path find in maze
https://www.cnblogs.com/mumuxinfei/p/4434244.html
"""
class Maze(object):
    BLOCK = 0
    EMPTY = 1
    START = 8
    EXIT = 9

    def __init__(self, matrix=None, width=None, height=None):
        if matrix is None:
            self.matrix = self.generate(width, height)
            self.height = height
            self.width = width
        else:
            self.matrix = matrix
            self.height = len(matrix)
            self.width = len(matrix[0])
            self.start = self.find_point(Maze.START)
            self.exit = self.find_point(Maze.EXIT)

        self.couter = {
            'pop': 0,
        }

    def generate(self, width, height):
        matrix = [[1 for x in range(width)] for y in range(height)]

        x, y = randrange(width), randrange(height)
        self.start = (x, y)
        matrix[y][x] = Maze.START

        while True:
            x, y = randrange(width), randrange(height)
            if matrix[y][x] == Maze.EMPTY:
                self.exit = (x, y)
                matrix[y][x] = Maze.EXIT
                break

        x1 = min(self.start[0], self.exit[0])
        x2 = max(self.start[0], self.exit[0])
        y1 = min(self.start[1], self.exit[1])
        y2 = max(self.start[1], self.exit[1])
        for i in range(int((x2 - x1) / 3)):
            x = randrange(x1, x2)
            if matrix[y1][x] == Maze.EMPTY:
                matrix[y1][x] = Maze.BLOCK
        for i in range(int((x2 - x1) / 3)):
            x = randrange(x1, x2)
            if matrix[y2][x] == Maze.EMPTY:
                matrix[y2][x] = Maze.BLOCK
        for i in range(int((y2 - y1) / 3)):
            y = randrange(y1, y2)
            if matrix[y][x1] == Maze.EMPTY:
                matrix[y][x1] = Maze.BLOCK
        for i in range(int((y2 - y1) / 3)):
            y = randrange(y1, y2)
            if matrix[y][x2] == Maze.EMPTY:
                matrix[y][x2] = Maze.BLOCK

        return matrix

    def display(self, path=None, queued=None, msg=''):
        if isinstance(path, tuple):
            path, path2 = path
        else:
            path2 = None

        if msg:
            print(msg)

        for y in range(self.height):
            line = ''
            for x in range(self.width):
                i = self.matrix[y][x]
                if i == Maze.BLOCK:
                    line += '   # '
                elif i == Maze.START:
                    line += '   ! '
                elif i == Maze.EXIT:
                    line += '   $ '
                elif path is not None and (x, y) in path:
                    line += ' %3d ' % (path.index((x, y)) + 1)
                elif path2 is not None and (x, y) in path2:
                    line += ' %3d ' % (path2.index((x, y)) + 1)
                elif queued is not None and (x, y) in queued:
                    line += '   . '
                else:
                    line += '     '
            print(line)

    def find_point(self, point_type):
        for y in range(self.height):
            for x in range(self.width):
                if self.matrix[y][x] == point_type:
                    return x, y
        raise Exception("Point not found")

    def is_valid_point(self, x, y):
        return (x >= 0 and x < self.width and
                y >= 0 and y < self.height and
                self.matrix[y][x] != Maze.BLOCK)

    def around(self, x, y):
        ps = []

        if self.is_valid_point(x, y - 1): # up
            ps.append((x, y - 1))
        if self.is_valid_point(x, y + 1): # down
            ps.append((x, y + 1))
        if self.is_valid_point(x + 1, y): # right
            ps.append((x + 1, y))
        if self.is_valid_point(x - 1, y): # left
            ps.append((x - 1, y))

        return ps

    def find_path_bfs(self):
        self.couter['pop'] = 0
        queued = set([self.start])
        queue = [[self.start]]
        while queue:
            path = queue.pop(0)
            self.couter['pop'] += 1

            # Traverse around points
            for p in self.around(*path[-1]):
                # Path found
                if p == self.exit:
                    return path + [p], queued.union([p])

                # Push queue
                if p not in queued:
                    queued.add(p)
                    queue.append(path + [p])

        return None, queued

    def find_path_bidirectional_bfs(self):
        self.couter['pop'] = 0
        queued_start = set([self.start])
        queued_exit = set([self.exit])
        queue = [
            ('start', [self.start]),
            ('exit', [self.exit])
        ]
        while queue:
            type, path = queue.pop(0)
            self.couter['pop'] += 1

            # Traverse around points
            for p in self.around(*path[-1]):
                # Path found
                if ((type == 'start' and p in queued_exit) or
                    (type == 'exit' and p in queued_start)):
                    for _, path2 in queue:
                        if path2[-1] == p:
                            break
                    return (path, path2), queued_start.union(queued_exit)

                # Push queue
                if type == 'start' and p not in queued_start:
                    queued_start.add(p)
                    queue.append((type, path + [p]))
                elif type == 'exit' and p not in queued_exit:
                    queued_exit.add(p)
                    queue.append((type, path + [p]))

        return None, queued_start.union(queued_exit)

    def find_path_a_star(self):
        self.couter['pop'] = 0

        def h_heuristic(x, y):
            return abs(x - self.exit[0]) + abs(y - self.exit[1])

        costs = {self.start: 1}
        queue = PriorityQueue()
        queue.put((h_heuristic(*self.start) + 1, [self.start]))
        while not queue.empty():
            _, path = queue.get_nowait()
            cost = len(path) + 1
            self.couter['pop'] += 1

            # Traverse around points
            for p in self.around(*path[-1]):
                if p == self.exit:
                    return path + [p], costs.keys()

                # Push queue
                if p not in costs or cost < costs[p]:
                    costs[p] = cost
                    queue.put((h_heuristic(*p) + cost, path + [p]))

        return None, costs.keys()

    @classmethod
    def main(cls):
        if 0:
            maze = Maze([
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 9, 1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            ])
        else:
            maze = Maze(matrix = [
                [1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0],
                [1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0],
                [1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1],
                [1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1],
                [1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1],
                [1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1],
                [1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1],
                [1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1],
                [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1],
                [1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 1, 0, 9],
            ])

        for i in range(1):
            maze = Maze(width=40, height=15)

            # BFS
            path, queued = maze.find_path_bfs()
            maze.display(path, queued, 'BFS Solution, %d steps, %d queued, %s' % (len(path), len(queued), maze.couter))
            sbfs = len(path)

            # Bidirectional BFS
            paths, queued = maze.find_path_bidirectional_bfs()
            maze.display(paths, queued, 'Bidirectional BFS, %d steps, %d queued, %s' % (len(paths[0]) + len(paths[1]), len(queued), maze.couter))
            sbibfs = len(paths[0]) + len(paths[1])

            # A Star
            path, queued = maze.find_path_a_star()
            maze.display(path, queued, 'A Star Solution, %d steps, %d queued, %s' % (len(path), len(queued), maze.couter))
            sastar = len(path)


if __name__=='__main__':
    Maze.main()
