#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define MAGIC 0xfeedbeaf

typedef struct _ShmBlock {
    unsigned int magic;
    key_t shmkey;
    int shmid;
    unsigned int count;
    size_t len;
    char *ptr;
    char data[1];
} ShmBlock;

ShmBlock *shm_attach(key_t key, size_t size)
{
    int shmid;
    void *shmp;
    ShmBlock *blk;

    // Get shmid
    shmid = shmget(key, size, IPC_CREAT);
    printf("shmid: %d\n", shmid);

    // Attach
    shmp = shmat(shmid, NULL, 0);
    printf("shm ptr: 0x%x\n", shmp);

    // Verify block
    blk = (ShmBlock *)shmp;
    if (blk->magic != MAGIC) {
        // Init
        blk->magic = MAGIC;
        blk->shmkey = key;
        blk->shmid = shmid;
        blk->count = 0;
        blk->len = size - sizeof(ShmBlock) + 1;
        blk->ptr = blk->data;

        // Zero-fill data
        for (blk->ptr = blk->data; blk->ptr < blk->data + blk->len; ++blk->ptr) {
            *blk->ptr = '\0';
        }
    }
    ++blk->count;
    blk->ptr = blk->data;

    return blk;
}

int shm_deattach(ShmBlock *blk)
{
    return shmdt(blk);
}

int main(int argc, char *argv[])
{
    key_t key;
    size_t size;
    ShmBlock *blk;

    key = 2134;
    size = 4096;
    blk = shm_attach(key, size);

    // Display block
    printf("===== Share Block Info =====\n");
    printf("magic:   %x\n", blk->magic);
    printf("shmkey:  %d\n", blk->shmkey);
    printf("shmid:   %d\n", blk->shmid);
    printf("count:   %u\n", blk->count);
    printf("length:  %u\n", blk->len);
    printf("pointer: %u\n", blk->ptr - blk->data);
    printf("data:    %s\n", blk->data);

    // strcpy(blk->data, "Hello, world!");

    shm_deattach(blk);

    return 0;
}
