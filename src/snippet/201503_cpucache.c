#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define ARRSIZ 67108864

#define TIMER_RESET gettimeofday(&tv_begin, NULL)

#define TIMER_DECLARE struct timeval tv_begin, tv_end, diff

#define TIMER_INIT TIMER_DECLARE; TIMER_RESET;

#define TIMER_DIFF do { \
    gettimeofday(&tv_end, NULL); \
    diff.tv_sec = tv_end.tv_sec - tv_begin.tv_sec; \
    diff.tv_usec = tv_end.tv_usec - tv_begin.tv_usec; \
    if (tv_end.tv_usec < tv_begin.tv_usec) { \
        diff.tv_sec -= 1; \
        diff.tv_usec += 1000000; \
    } \
} while (0)

#define TIMER_SHOW do { \
    printf("cost: %ld.%06ld\n", diff.tv_sec, diff.tv_usec); \
} while (0)

void traverse(int *arr, int size, int step, int times)
{
    int i, mask;

    mask = size - 1;
    TIMER_INIT;
    for (i = 0; i < times; ++i)
        arr[(i * step) & mask] *= 3;
    TIMER_DIFF;

    printf("Step: %4d\t", step);
    TIMER_SHOW;
}

void traverse_b(int *arr, int size, int step, int times)
{
    int i, mask;

    mask = size - 1;
    TIMER_INIT;
    for (i = 0; i < times; ++i)
        arr[(i * step) & mask] *= 3;
    TIMER_DIFF;

    if (size >= 1024 * 1024) {
        printf("Size: %4dm\t", size / 1024 / 1024);
    } else {
        printf("Size: %4dk\t", size / 1024);
    }
    TIMER_SHOW;
}

int main(int argc, char *argv[])
{
    int i, step, size, *arr;
    TIMER_INIT;

    /* 实验了文章http://coolshell.cn/articles/10249.html提到的例子 */

    /* 示例1,2：不同步长的区别 */
    if (1) {
        arr = (int *)malloc(sizeof(int) * ARRSIZ);
        for (step = 1; step < 2048; step *= 2) {
            traverse(arr, ARRSIZ, step, ARRSIZ);
        }
        free(arr);
    }

    /* 示例3：数组大小不同的区别 */
    if (0) {
        for (size = 1024; size < 1024 * 1024 * 1024; size *= 2) {
            arr = (int *)malloc(sizeof(int) * size);
            traverse_b(arr, size, 16, ARRSIZ);
            free(arr);
        }
    }

    /* 示例4：指令级并发 */
    if (0) {
        arr = (int *)malloc(sizeof(int) * 3);
        TIMER_RESET;
        for (i = 0; i < ARRSIZ; i++) { arr[0]++; arr[0]++; arr[0]++; }
        TIMER_DIFF; printf("000 "); TIMER_SHOW;

        TIMER_RESET;
        for (i = 0; i < ARRSIZ; i++) { arr[0]++; arr[1]++; arr[2]++; }
        TIMER_DIFF; printf("012 "); TIMER_SHOW;
    }

    /* 示例5：缓存映射方案 */

    return 0;
}
