#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    size_t bits;
    size_t bytes;
    unsigned char arr[];
} bmapHandler;

bmapHandler *bmapAlloc(size_t bits) {
    size_t bytes;
    bmapHandler *bh;

    bytes = bits / 8 + 1;
    bh = (bmapHandler *) malloc(sizeof(bmapHandler) + bytes);

    bh->bits = bits;
    bh->bytes = bytes;
    memset(bh->arr, 0, bh->bytes);

    return bh;
}

void bmapFree(bmapHandler *bh) {
    free(bh);
}

int bmapGet(bmapHandler *bh, int i) {
    unsigned char byte;

    byte = bh->arr[i / 8];
    switch (i % 8) {
        case 0: return (byte & 1);
        case 1: return (byte & 2);
        case 2: return (byte & 4);
        case 3: return (byte & 8);
        case 4: return (byte & 16);
        case 5: return (byte & 32);
        case 6: return (byte & 64);
        case 7: return (byte & 128);

        default: return -1;
    }
}

void bmapSet(bmapHandler *bh, int i) {
    unsigned char *byte;

    byte = &bh->arr[i / 8];
    switch (i % 8) {
        case 0: *byte |= 1; break;
        case 1: *byte |= 2; break;
        case 2: *byte |= 4; break;
        case 3: *byte |= 8; break;
        case 4: *byte |= 16; break;
        case 5: *byte |= 32; break;
        case 6: *byte |= 64; break;
        case 7: *byte |= 128; break;
    }
}

void bmapDel(bmapHandler *bh, int i) {
    unsigned char *byte;

    byte = &bh->arr[i / 8];
    switch (i % 8) {
        case 0: *byte &= ~1; break;
        case 1: *byte &= ~2; break;
        case 2: *byte &= ~4; break;
        case 3: *byte &= ~8; break;
        case 4: *byte &= ~16; break;
        case 5: *byte &= ~32; break;
        case 6: *byte &= ~64; break;
        case 7: *byte &= ~128; break;
    }
}

int main(int argc, char *argv[]) {
    int i;
    bmapHandler *bh;

    bh = bmapAlloc(INT_MAX);

    if (0) { /* Test */
        for (i = 0; i < 10; i++) {
            printf("bmapGet(%d) %d\n", i, bmapGet(bh, i));
            bmapSet(bh, i);
            printf("bmapGet(%d) %d\n", i, bmapGet(bh, i));
        }

        for (i = 0; i < 10; i++) {
            printf("bmapGet(%d) %d\n", i, bmapGet(bh, i));
            bmapDel(bh, i);
            printf("bmapGet(%d) %d\n", i, bmapGet(bh, i));
        }
    }

    if (1) { /* Benchmark */
        for (i = 0; i < INT_MAX / 2; i++) {
            bmapGet(bh, i);
            bmapSet(bh, i);
            bmapDel(bh, i);
        }
    }

    bmapFree(bh);
    
    return 0;
}
