#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <string.h>

int readarg(char *input, char *arg)
{
    char *begin;

    for (begin = input; *input && *input != ' '; ++input)
        ;

    strncpy(arg, begin, input - begin);
    arg[input - begin] = '\0';

    return input - begin;
}

void *mmap_map(const char *filename, size_t filesize)
{
    int fd;
    void *pmap;

    fd = open(filename, O_RDWR, 0);
    pmap = mmap(NULL, filesize, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);

    if (pmap == MAP_FAILED) {
        printf("mmap failed, file: %s\n", filename);
    } else {
        printf("mmap addr: 0x%08x, file: %s\n", pmap, filename);
    }

    return pmap;
}

int mmap_unmap(void *pmap, size_t length)
{
    printf("mmap unmap\n");
    return munmap(pmap, length);
}

int mmap_sync(void *pmap, size_t length)
{
    printf("mmap sync\n");
     return msync(pmap, length, MS_SYNC);
}

void mmap_read(void *pmap, size_t length)
{
    void *p;

    printf("{Begin of mmap file}\n");
    for (p = pmap; p < pmap + length; ++p) {
        putchar(*(char *)p);
    }
    printf("{End of mmap file}\n");
}

void mmap_modify(void *pmap)
{
    memcpy(pmap, "hello, world", 12);
}

int main(int argc, char *argv[])
{
    int arglen, linelen;
    size_t filesize;
    struct stat st;
    void *pmap;
    char line[BUFSIZ], filename[BUFSIZ], arg[16], *lineptr;

    // Fileinfo
    strcpy(filename, "/home/wangyuchen/workspace/mmap/data.bin");
    stat(filename, &st);
    filesize = st.st_size;

    // Read command
    printf("> ");
    while (lineptr = fgets(line, BUFSIZ, stdin)) {
        linelen = strlen(line);
        line[linelen - 1] = '\0';

        // Command
        arglen = readarg(lineptr, arg);

        if (strcmp("map", arg) == 0) {
            pmap = mmap_map(filename, filesize);
        } else if (strcmp("unmap", arg) == 0) {
            mmap_unmap(pmap, filesize);
        } else if (strcmp("sync", arg) == 0) {
            mmap_sync(pmap, filesize);
        } else if (strcmp("read", arg) == 0) {
            mmap_read(pmap, filesize);
        } else if (strcmp("mod", arg) == 0) {
            mmap_modify(pmap);
        } else if (strcmp("exit", arg) == 0) {
            return 0;
        } else {
            printf("Unknown command \"%s\" in \"%s\"\n", arg, line);
        }

        printf("> ");
    }

    return 0;
}
