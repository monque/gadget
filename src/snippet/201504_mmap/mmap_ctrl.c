#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>

#define MAGICNUM 987234610
#define CTRLFILE "/tmp/monque-ctrl-context"

static struct timespec ti = {0, 100000000};

struct CtrlContext {
    int magic;
    char req;  /* has request */
    char data[1024];
};

int main(int argc, char *argv[])
{
    int fd;
    struct CtrlContext *ctx;

    /* args */
    if (argc < 2) {
        return 1;
    }
    
    /* open mmap */
    fd = open(CTRLFILE, O_RDWR | O_CREAT, DEFFILEMODE);
    ctx = (struct CtrlContext *) mmap(NULL, sizeof(struct CtrlContext), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (ctx == MAP_FAILED) {
        printf("mmap failed, file: %s\n", CTRLFILE);
        return 2;
    }

    /* init */
    if (ctx->magic != MAGICNUM) {
        ctx->magic = MAGICNUM;
        ctx->req = 0;
        *ctx->data = '\0';
    }

    /* run */
    if (!strcmp("ext", argv[1])) {
        while (1) {
            /* check */
            if (ctx->req) {
                fprintf(stderr, "data: %s\n", ctx->data);
                ctx->req = 0;
            }

            nanosleep(&ti, NULL);
        }
    } else {
        ctx->req = 1;
        strcpy(ctx->data, argv[1]);
    }

    return 0;
}
