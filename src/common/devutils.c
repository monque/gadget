#include "devutils.h"

void showVarBits(const void *var, size_t size)
{
    static const unsigned char bitemask[] = {
        0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01
    };
    int i;
    unsigned char *p;

    p = (unsigned char *) var;
    for (p = p + size - 1; p >= (unsigned char *) var; p--) { /* little endian */
        for (i = 0; i < 8; i++) {
            if (*p & bitemask[i])
                printf(MGC_BYELLOW "1");
            else
                printf(MGC_YELLOW "0");
        }
        printf(" ");
    }
    printf(MGC_RESET);
}
