#ifndef DEVUTILS_H
#define DEVUTILS_H

#include <stdio.h>
#include <stdlib.h>

/* http://ascii-table.com/ansi-escape-sequences.php */

#define MGC_RESET       "\x1b[0m"

#define MGC_BLACK       "\x1b[0;30m"
#define MGC_RED         "\x1b[0;31m"
#define MGC_GREEN       "\x1b[0;32m"
#define MGC_YELLOW      "\x1b[0;33m"
#define MGC_BLUE        "\x1b[0;34m"
#define MGC_MAGENTA     "\x1b[0;35m"
#define MGC_CYAN        "\x1b[0;36m"
#define MGC_WHITE       "\x1b[0;37m"

#define MGC_BBLACK      "\x1b[1;30m"
#define MGC_BRED        "\x1b[1;31m"
#define MGC_BGREEN      "\x1b[1;32m"
#define MGC_BYELLOW     "\x1b[1;33m"
#define MGC_BBLUE       "\x1b[1;34m"
#define MGC_BMAGENTA    "\x1b[1;35m"
#define MGC_BCYAN       "\x1b[1;36m"
#define MGC_BWHITE      "\x1b[1;37m"

#define showBits(var) {                         \
	typeof(var) _val_ = var;					\
	printf(MGC_MAGENTA #var MGC_CYAN " : ");	\
	showVarBits(&_val_, sizeof _val_);			\
	printf(MGC_RESET "\n");					    \
}

#define printVar(format, var) printf(MGC_MAGENTA #var MGC_CYAN " = " MGC_YELLOW format MGC_RESET "\n", var)
#define pint(var)       printVar("%d", var)
#define puint(var)      printVar("%u", var)
#define plong(var)      printVar("%ld", var)
#define pfloat(var)     printVar("%f", var)
#define pdouble(var)    printVar("%lf", var)

void showVarBits(const void *var, size_t size);

#endif
