#!/bin/bash
cd `dirname $0`

# Common
setxkbmap -option terminate:ctrl_alt_bksp
xset r rate 300 50

# Restore
if [ "$1" == "r" ] || [ "$1" == "restore" ]; then
    setxkbmap -option
    xmodmap - <<end-of-heredoc
keycode   9 = Escape NoSymbol Escape
keycode  49 = grave asciitilde grave asciitilde
keycode 105 = Control_R NoSymbol Control_R
keycode 134 = Super_R NoSymbol Super_R
keycode  26 = e E e E
keycode  39 = s S s S
keycode  40 = d D d D
keycode  41 = f F f F
end-of-heredoc

# Hack
elif [ "$1" == "h" ] || [ "$1" == "hack" ]; then
    setxkbmap -option ctrl:swapcaps
    xmodmap - <<end-of-heredoc
! Swap Esc, Grv
keycode   9 = grave asciitilde grave asciitilde
keycode  49 = Escape NoSymbol Escape
! Set R-Ctrl, R-Super + esdf to Arrow keys
keycode 105 = Mode_switch NoSymbol Mode_switch
keycode 134 = Mode_switch NoSymbol Mode_switch
keycode  26 = e E Up E
keycode  39 = s S Left S
keycode  40 = d D Down D
keycode  41 = f F Right F
end-of-heredoc

fi
