#!/bin/sh

d() { echo `date +'%Y-%m-%d %H:%M:%S'` $@; }
r() { d $@; $@; ret=$?; }

main()
{
    # Wake up in seconds
    d "On wake-up begin"

    # Scan ssids
    ssids=`/System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Resources/airport -I | grep ' SSID' | awk '{print $2}'`
    while [ -z "$ssids" ]; do
        sleep 1
        ssids=`/System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Resources/airport -s | awk '{print $1}' | sort | uniq`
    done
    d "ssid: $ssids"

    # Detect location
    if [[ $ssids = *"Ambelia"* ]]; then
        location="DongBa"
    elif [[ $ssids = *"Braavos"* ]]; then
        location="WangJing"
    elif [[ $ssids = *"ke.com"* ]]; then
        location="LianJia"
    else
        location="Unknown"
    fi
    osascript -e "display notification \"Location: $location\" with title \"On wakeup\""
    d "Location: $location"

    # Action
    if [ "$location" == "WangJing" ]; then
        r networksetup -setproxyautodiscovery "Wi-Fi" off
        r networksetup -setautoproxystate "Wi-Fi" on
        r networksetup -setautoproxyurl "Wi-Fi" "https://mo47.com/agw-wj.pac"
    elif [ "$location" == "DongBa" ]; then
        r networksetup -setproxyautodiscovery "Wi-Fi" off
        # r networksetup -setwebproxystate "Wi-Fi" on
        # r networksetup -setwebproxy "Wi-Fi" 192.168.32.149 7890
        # r networksetup -setsecurewebproxystate "Wi-Fi" on
        # r networksetup -setsecurewebproxy "Wi-Fi" 192.168.32.149 7890
        # r networksetup -setsocksfirewallproxystate "Wi-Fi" on
        # r networksetup -setsocksfirewallproxy "Wi-Fi" 192.168.32.149 7890
    elif [ "$location" == "LianJia" ]; then
        r networksetup -setproxyautodiscovery "Wi-Fi" off
        r networksetup -setautoproxystate "Wi-Fi" on
        r networksetup -setautoproxyurl "Wi-Fi" "https://mo47.com/agw-local.pac"
    else
        r networksetup -setproxyautodiscovery "Wi-Fi" off
        r networksetup -setautoproxystate "Wi-Fi" on
        r networksetup -setautoproxyurl "Wi-Fi" "https://mo47.com/agw-local.pac"
    fi

    d "On wake-up end"
}


main
