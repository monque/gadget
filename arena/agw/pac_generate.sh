cat pac_template.js | \
    sed 's/{REPLACEMENT}/PROXY 192.168.95.106:8118; SOCKS5 192.168.95.106:1080/' \
    > agw-wj.pac

cat pac_template.js | \
    sed 's/{REPLACEMENT}/PROXY 192.168.32.149:7890/' \
    > agw-db.pac

cat pac_template.js | \
    sed 's/{REPLACEMENT}/SOCKS5 127.0.0.1:1080/' \
    > agw-local.pac
