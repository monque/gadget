function FindProxyForURL(url, host) {
    for (var i in proxyPatterns) {
        if (proxyPatterns[i].test(host)) {
            return proxy;
        }
    }

    return 'DIRECT';
}

var proxy = "{REPLACEMENT}";

var proxyPatterns = [
    // Google
    /(.*\.|^)google\.com(\.\w+)?/,
    /(.*\.|^)google\.co\.(\w+)/,
    /(.*\.|^)googleapis\.com/,
    /(.*\.|^)googlecode\.com/,
    /(.*\.|^)googleusercontent\.com/,
    /(.*\.|^)googleadservices\.com/,
    /(.*\.|^)googlesource\.com/,
    /(.*\.|^)google-analytics\.com/,
    /(.*\.|^)gstatic\.com/,
    /(.*\.|^)ggpht\.com/,
    /(.*\.|^)appspot\.com/,
    /(.*\.|^)blogspot\.com/,
    /(.*\.|^)chrome\.com/,
    /(.*\.|^)android\.com/,
    /(.*\.|^)googletagservices\.com/,
    /(.*\.|^)googletagmanager\.com/,
    /(.*\.|^)googlesyndication\.com/,
    /goo\.gl/,

    // Google Android
    /(.*\.|^)gvt1\.com/,
    /(.*\.|^)gvt(\d)\.com/, // gvt1.com, gvt2.com, ...

    // Google Org
    /(.*\.|^)golang\.org/,
    /(.*\.|^)tensorflow\.org/,

    // Youtube
    /(.*\.|^)youtube\.com/,
    /(.*\.|^)youtube-nocookie\.com/,
    /(.*\.|^)youtu\.be/,
    /(.*\.|^)ytimg\.com/,
    /(.*\.|^)googlevideo\.com/,

    // Twitter
    /(.*\.|^)twitter\.com/,
    /(.*\.|^)twimg\.com/,
    /(.*\.|^)t\.co/,

    // Dropbox
    /(.*\.|^)dropbox\.com/,
    /(.*\.|^)dropboxstatic\.com/,
    /(.*\.|^)dropboxusercontent\.com/,

    // Facebook
    /(.*\.|^)facebook\.com/,
    /(.*\.|^)facebook\.net/,
    /(.*\.|^)fbcdn\.net/,

    // Amazon
    /(.+\.|^)s3\.amazonaws\.com/,
    /(.*\.|^)cloudfront\.net/,
    /images-na\.ssl-images-amazon\.com/,

    // CDN
    /(.*\.|^)akamai\.net/,
    /(.*\.|^)rackspacecloud\.com/,
    /(.*\.|^)rtrcdn\.com/,
    /(.*\.|^)rpcdn\.com/,
    /(.*\.|^)imagevenue\.com/,
    /(.*\.|^)vox-cdn\.com/,
    /(.*\.|^)cdn\.snowboarding\.transworld\.net/,
    /(.*\.|^)global\.ssl\.fastly\.net/,
    /(.*\.|^)crwdcntrl\.net/,
    /ad-cdn\.technoratimedia\.com/,
    /(.*\.|^)cloundfront\.net/,
    /(.*\.|^)yimg\.com/,
    /(.*\.|^)line-scdn\.net/,

    // Pixnet
    /(.+\.|^)pixnet\.net/,
    /(.+\.|^)pixnet\.in/,
    /(.+\.|^)pixplug\.in/,
    /(.+\.|^)pixfs\.net/,

    // Snowboard
    /(.*\.|^)evo\.com/,
    /(.*\.|^)neversummer\.com/,

    // Wikipedia
    /(.*\.|^)wikimedia\.org/,
    /(.*\.|^)wikipedia\.org/,

    // Github
    /(.+\.|^)githubusercontent\.com/,
    /assets-cdn\.github\.com/,

    /(.*\.|^)tumblr\.com/, /(.*\.|^)t\.umblr\.com/,
    /(.*\.|^)garmin\.com/,
    /(.*\.|^)trainingpeaks\.com/,
    /(.*\.|^)bitcointalk\.org/, /(.*\.|^)bitcoin\.org/,
    /(.*\.|^)instagram\.com/, /(.*\.|^)cdninstagram\.com/,
    /(.*\.|^)feedly\.com/,
    /(.*\.|^)vimeo\.com/,
    /(.*\.|^)greatfire\.org/,
    /(.*\.|^)typekit\.com/,
    /(.*\.|^)slideshare\.net/,
    /(.*\.|^)bit\.ly/,
    /(.*\.|^)namecheap\.com/,
    /(.*\.|^)onedrive\.live\.com/,
    /(.*\.|^)theverge\.com/,
    /(.*\.|^)medium\.com/,
    /(.*\.|^)huobipro\.com/,
    /(.*\.|^)freeweibo\.com/,
    /(.*\.|^)inoreader\.com/,
    /(.*\.|^)wordpress\.com/,
    /(.*\.|^)gravatar\.com/,
    /(.*\.|^)deviantart\.com/,
    /(.*\.|^)nikecloud\.com/,
    /(.*\.|^)developer\.mozilla\.org/,
    /(.*\.|^)todoist\.com/,
    /(.*\.|^)quora\.com/,
    /(.*\.|^)shodan\.io/,
    /(.*\.|^)flaticon\.com/,
    /(.*\.|^)cnn\.com/,
    /(.*\.|^)nytimes\.com/,
    /(.*\.|^)v2ray\.com/,
    /(.*\.|^)bbc\.com/, /(.*\.|^)bbc\.co\.uk/,
    /(.*\.|^)cdn\.shopify\.com/,
    /(.*\.|^)engadget\.com/,
];
