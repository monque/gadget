CFLAGS = -g -Isrc/common src/common/devutils.c

.PHONY: default
default:
	@echo "Usage: ./makerun SOURCE-FILE"

.PHONY: clean
clean:
	rm -fr build
	find . -name '*.out' | xargs rm -fv

build/csapp/%: src/csapp/%.c
	@mkdir -p `dirname $@`
	$(CC) $(CFLAGS) -Isrc/csapp -o $@ $<

build/%: src/%.c
	@mkdir -p `dirname $@`
	$(CC) $(CFLAGS) -o $@ $<
